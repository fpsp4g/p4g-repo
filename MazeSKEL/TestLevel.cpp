#include "TestLevel.h"
#include "LevelManager.h"

void TestLevel::Init() {
	floorTexture = 32;
	ceilingTexture = 32;

	CreateMap("data/levels/Test.png");
}

void TestLevel::SwitchLevel(int id)
{
	if (id == 1) LevelManager::GetMyLevelManager()->SwitchLevel("Level1",1);
	//if (id == 2) LevelManager::GetMyLevelManager()->SwitchLevel("Level2",1);
}
