#include "TriggerDoor.h"

TriggerDoor::TriggerDoor(float x, float z, float yRotation) : DynamicDoor(x, z, yRotation){
	tint = DirectX::SimpleMath::Vector3(0.357f, 0.09f, 0.071f);
}

void TriggerDoor::Trigger(bool pressed)
{
	
	isOpening = pressed;

}

void TriggerDoor::Update(float dTime)
{
	//----------------------
	UpdateAnimation(dTime);
}

