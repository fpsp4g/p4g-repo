#include "WaterBlock.h"

WaterBlock::WaterBlock() : time(0),arrayIndex(0),ANIMATE_TIME(0.2f) {
	arraySize = sizeof(textures) / sizeof(textures[0]);

	floorColour = 0x0033FF;
	floorTextureID = textures[arrayIndex];
}

void WaterBlock::Update(float dTime)
{
	time += dTime;

	if (time >= ANIMATE_TIME) {
		time = 0;

		arrayIndex++;
		if (arrayIndex >= arraySize)
			arrayIndex = 0;

		SetFloorTexture(textures[arrayIndex]);
	}
}

