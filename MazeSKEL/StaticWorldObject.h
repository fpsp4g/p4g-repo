#ifndef StaticWorldObjectH
#define StaticWorldObjectH

#include "WorldObject.h"
#include "Node.h"

class StaticWorldObject :
	public WorldObject
{
public:
	StaticWorldObject();
	~StaticWorldObject();
private:
	//Node currentNode;
};
#endif