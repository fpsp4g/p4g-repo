#ifndef DECALRENDERERH
#define DECALRENDERERH

#include "TextureRegionHandler.h"
#include "FX.h"
#include <DirectXMath.h>

using namespace DirectX::SimpleMath;

class DecalRenderer {

public:

	void Release();
	void Init(ID3D11ShaderResourceView* spriteSheetTexture, TextureRegionHandler* textureRegionHandler);
	inline void Render(const Vector3& position,const Vector3& scale, const Vector3& rotation, const Vector3& tint, int textureIndex);
	void Render(const class DecalEntity* decal);
	void Flush();


private:

	const int MAX_VERTICES = 1024;

	std::vector<VertexPosNormTexColor> vertices;
	std::vector<UINT> indices;
	int numIndices;

	MaterialExt material;						// Material used for rendering
	ID3D11Buffer* vertexBuffer = nullptr;		// Vertex buffer used for rendering
	ID3D11Buffer* indexBuffer = nullptr;		// Index buffer	used for rendering
	TextureRegionHandler* textureRegionHandler;

};

#endif