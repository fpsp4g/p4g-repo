#ifndef WorldObjectH
#define WorldObjectH

#include "Entity.h"

class WorldObject :
	public Entity
{
public:
	WorldObject();
	~WorldObject();
};
#endif