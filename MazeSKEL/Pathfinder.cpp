#include "Pathfinder.h"
#include <algorithm>
#include "SpecialTileBlock.h"
#include <random>
Pathfinder *Pathfinder::instance(nullptr);



void Pathfinder::FindPath(Node startPos, Node targetPos, Queue<Node>& pathToUse, bool* hasPathBeenFound) //calculate the path from the startPos to the targetPos in reverse (this is so that the queue will go in the correct order)
{
	while (pathToUse.length() != 0)
	{
		pathToUse.removeFirst();
	}
	
	//A* pathfinding algorithm
	int currentDist = 0;
	std::vector<NodeScore<Node>> closedSet;			//nodes that have been considered
	//pathToUse.addToFront(targetPos);		//add the target to the path, this will put it at the end of the path by the time that the pathfinder has finished its job
	
	//closedSet.push_back(NodeScore<Node>(&targetPos, 0, targetPos));
	Queue<NodeScore<Node>> openSet;	//the nodes that still need to be considered
	for (int i = 0; i < targetPos.LengthOfLinkedNodes(); i++)	//Add the nodes linked to the target to the open list, so we can use them in the algorithm
	{
		openSet.addToFront(NodeScore<Node>(targetPos.GetLinkedNodes()[i].node, targetPos.GetLinkedNodes()[i].scoreFromThisNode + currentDist, targetPos));
	}
	bool pathfound = false;
	
	while (!pathfound && openSet.length() > 0)      //while a path hasn't been found yet, and there is still a possibility of finding a path....
	{
		
		int currentLowestScore = 1000000;	//make the current lowest score a REALLY big number, so that there is guarenteed to be a score lower than it
		std::vector<NodeScore<Node>> lowestNode;
		float tempDist = currentDist;
		for (int i = 0; i < openSet.length(); i++)
		{
			float score = currentDist + openSet.getObjAtPos(i).scoreFromThisNode + openSet.getObjAtPos(i).node->getDistanceToNode(startPos); //score is a heuristic, distance from current position + distance to target
			
			if (score < currentLowestScore) //if the node has a lower score than that which has been stored, clear the list and put the new node in
			{
				currentDist = openSet.getObjAtPos(i).scoreFromThisNode;
				lowestNode.empty();
				lowestNode.push_back(openSet.getObjAtPos(i));
				currentLowestScore = score;
			}
			else if (score == currentLowestScore)   //if it has the same score, just add it to the list
			{
				lowestNode.push_back(openSet.getObjAtPos(i));
			}
		}
		 
		//std::vector<NodeScore<Node>> adjacentNodes;
		for (int i = 0; i < lowestNode.size(); i++)
		{
			closedSet.push_back(lowestNode[i]);
			openSet.deleteItem(lowestNode[i]);	//remove the node from the list of nodes to be considered
			
			if ((*lowestNode[i].node) == startPos) //if the node is the start, it means that we've found a viable path
			{
				
				pathfound = true;
			}
			else
			{
				//add any nodes connected to the lowest node to the list to check, so long as it isn't the lowest node itself
				std::vector<NodeScore<Node>> nodeToCheck;
				for (int link = 0; link < lowestNode[i].node->LengthOfLinkedNodes(); link++)
				{
					//if (*lowestNode[i].node != *lowestNode[i].node->GetLinkedNodes()[link].node)
					//{
						nodeToCheck.push_back(lowestNode[i].node->GetLinkedNodes()[link]);
						
					//}
				}
				for (int j = 0; j < nodeToCheck.size(); j++)	 //add all connected nodes to the open list
				{
					int count = 0;
					bool nodeFound = false;
					while (!nodeFound && count < closedSet.size())						//check if the 
					{
						if (*closedSet[count].node == *nodeToCheck[j].node)  //if the node isn't already in the closed list (i.e. it's already been considered
						{
							nodeFound = true;
						}
						count++;
					}
					if (!nodeFound)  //if the node isn't already in the closed list (i.e. it's already been considered)
					{
						NodeScore<Node> nodeToAdd(nodeToCheck[j].node, nodeToCheck[j].scoreFromThisNode + currentDist, *lowestNode[i].node);

						//try to update an extisting item in the open set, if there isn't one to update, add the      
						if (!openSet.updateItem(nodeToAdd))
							openSet.addToFront(nodeToAdd);
					}
				}
			}
		}
	}
	if (pathfound)    //if the player was found 
	{
		//reverse search through the list to find the next tile to move to
		bool found = false;
		
		while (!found)
		{
			NodeScore<Node> currentStepInPath(&startPos, 0, Node(0,0,false));
			if (closedSet.size() != 0) {
				currentStepInPath = closedSet[closedSet.size() - 1];
				closedSet.pop_back();
				pathToUse.addToEnd(*currentStepInPath.node);
			}
			else
			{
				found = true;
			}
			if (*currentStepInPath.node == targetPos)
			{
				found = true;
			}
			else
			{
				if (closedSet.size() != 0) {
					while (closedSet.size() != 0 && currentStepInPath.prevNode != *closedSet[closedSet.size() - 1].node && *closedSet[closedSet.size() - 1].node != targetPos )
					{
						closedSet.pop_back();
					}
				}
			}
		}
		(*hasPathBeenFound) = true;
	}

}

void Pathfinder::DrawStraightLine(Node start, Node target, bool* canSearch, bool* foundVal)	//used to check whether or not an enemy can see the player 
//NOTE: the line drawer will give up after 15 spaces, so the enemy won't be able to see the player if they are too far away
{
	bool found = false;
	bool giveUp(false);
	Node current(start);
	int count = 0;
	while (!found && ! giveUp && count < 15) 
	{
		bool foundAPoint(false);
		if (target.getX() != current.getX() && target.getY() != current.getY()) //if the target is currently on a diagonal to the start...
		{
			
			if (target.getX() < current.getX()) 
			{
				if (target.getY() < current.getY()) //target x is less than current x, target y is less than current y
				{
					for (int i = 0; i < current.GetLinkedDiagonals().size(); i++)
					{
						if (current.GetLinkedDiagonals().at(i)->getX() < current.getX() && current.GetLinkedDiagonals().at(i)->getY() < current.getY())
						{
							foundAPoint = true;
							current = *current.GetLinkedDiagonals().at(i);
						}
					}
				}
				else //target x is less than current x, target y is greater than current y
				{
					for (int i = 0; i < current.GetLinkedDiagonals().size(); i++)
					{
						if (current.GetLinkedDiagonals().at(i)->getX() < current.getX() && current.GetLinkedDiagonals().at(i)->getY() > current.getY())
						{
							foundAPoint = true;
							current = *current.GetLinkedDiagonals().at(i);
						}
					}
				}
			}
			else 
			{
				if (target.getY() < current.getY()) //target x is greater than current x, target y is less than current y
				{
					for (int i = 0; i < current.GetLinkedDiagonals().size(); i++)
					{
						if (current.GetLinkedDiagonals().at(i)->getX() > current.getX() && current.GetLinkedDiagonals().at(i)->getY() < current.getY())
						{
							foundAPoint = true;
							current = *current.GetLinkedDiagonals().at(i);
						}
					}
				}
				else //target x is greater than current x, target y is less than current y
				{
					for (int i = 0; i < current.GetLinkedDiagonals().size(); i++)
					{
						if (current.GetLinkedDiagonals().at(i)->getX() > current.getX() && current.GetLinkedDiagonals().at(i)->getY() > current.getY())
						{
							foundAPoint = true;
							current = *current.GetLinkedDiagonals().at(i);
						}
					}
				}
			}
			
		}
		else//the target is on the same axis as the start (either x or y)
		{
			if (target.getX() < current.getX())
			{
				for (int i = 0; i < current.GetLinkedNodes().size(); i++)
				{
					if (current.GetLinkedNodes().at(i).node->getX() < current.getX())
					{
						foundAPoint = true;
						current = *current.GetLinkedNodes().at(i).node;
					}
				}
			}
			else if (target.getX() > current.getX())
			{
				for (int i = 0; i < current.GetLinkedNodes().size(); i++)
				{
					if (current.GetLinkedNodes().at(i).node->getX() > current.getX())
					{
						foundAPoint = true;
						current = *current.GetLinkedNodes().at(i).node;
					}
				}
			}
			else if (target.getY() < current.getY())
			{
				for (int i = 0; i < current.GetLinkedNodes().size(); i++)
				{
					if (current.GetLinkedNodes().at(i).node->getY() < current.getY())
					{
						foundAPoint = true;
						current = *current.GetLinkedNodes().at(i).node;
					}
				}
			}
			else if (target.getY() > current.getY())
			{
				for (int i = 0; i < current.GetLinkedNodes().size(); i++)
				{
					if (current.GetLinkedNodes().at(i).node->getY() > current.getY())
					{
						foundAPoint = true;
						current = *current.GetLinkedNodes().at(i).node;
					}
				}
			}
		}
		if (!foundAPoint) //exit the loop if you couldn't find a viable path
		{
			giveUp = true;
		}
		else if (current.getX() == target.getX() && current.getY() == target.getY()) {
			found = true;
		}
		count++;
	}

	*foundVal = found;
	*canSearch = true;
}

Vector3 Pathfinder::FindRandomWalkableTilePos(float dTime, Node node)
{
	SeedRandom(int(dTime * 10000000));
	int iterations = GetRandom(5, 15);
	Node temp = node;
	for (int i = 0; i < iterations; i++)
	{
		temp = *(temp.GetLinkedNodes().at(GetRandom(0, temp.GetLinkedNodes().size() - 1)).node);
	}
	return Vector3(temp.getX(), 0, temp.getY());
}