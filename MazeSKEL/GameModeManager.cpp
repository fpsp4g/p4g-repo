#include "GameModeManager.h"



GameModeManager::GameModeManager() : game(nullptr)
{
}




GameModeManager::~GameModeManager()
{

}

GameModeManager * GameModeManager::GetInstance()
{
	static GameModeManager instance;
	return &instance;
}


void GameModeManager::SwapMode(int Mode)
{
	//If we don't have a temp mode waiting then set it
	if (temp == nullptr) {
		if (Mode == 1) {
			temp = new PlayingGameMode(p_levelManager);
		}
		else if (Mode == 2) {
			temp = new LeaderboardMode();
		}
		else if (Mode == 0) {
			temp = new TitleMode();
		}
		else if (Mode == 3) {
			temp = new GameOverMode();
		}
		else if (Mode == 4)
		{
			temp = new GameWonMode();
		}
		else if (Mode == 9) {
			temp = new LoadingGameMode();
		}
		else if (Mode == 8) {
			temp = new ControlsMode();
		}
	}
	//We are waiting to update game mode
	else {

		GameMode* current = game->currentGameMode;
		if (current != nullptr) {
			current->Release();
			delete current;
			current = nullptr;		// Always remember to put this back to nullptr - Tom
		}


		delete current;
		current = temp;
		temp = nullptr;
		


		game->currentGameMode = current;

		if (game->currentGameMode != nullptr) {
			game->currentGameMode->Initialise();
		}
	}
}

void GameModeManager::SetGame(Game * myGame)
{
	game = myGame;
}

bool GameModeManager::AwaitingSwap()
{
	return temp != nullptr;
}
