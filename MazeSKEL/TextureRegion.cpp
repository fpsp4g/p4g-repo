/*******************************************************
* Copyright (C) 2018 Thomas Dunn tom1998dunn@hotmail.com
*
* This file is part of P4G Assignment.
*
* This code can not be copied and/or distributed without the express
* permission of Thomas Dunn
*******************************************************/

#include "TextureRegion.h"

using namespace DirectX::SimpleMath;

TextureRegion::TextureRegion(ID3D11ShaderResourceView* texture_, float x, float y, float width, float height) : texture(texture_) {
	Vector2 textureDim = FX::GetMyFX()->mCache.GetDimensions(texture);
	minU = x / textureDim.x;
	minV = y / textureDim.y;
	maxU = width / textureDim.x;
	maxV = height / textureDim.y;
}

float TextureRegion::GetMinU() const {
	return minU;
}
float TextureRegion::GetMinV() const {
	return minV;
}
float TextureRegion::GetMaxU() const {
	return maxU;
}
float TextureRegion::GetMaxV() const {
	return maxV;
}