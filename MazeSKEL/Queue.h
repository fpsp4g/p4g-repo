#ifndef QueueH
#define QueueH

//-----------------QUEUEITEM STUFF--------------------
template<typename TQI>
class QueueItem
{
public:
	QueueItem();
	QueueItem(const QueueItem<TQI>&);
	QueueItem(const TQI&);
	TQI item_;
	QueueItem<TQI>* next_;
};

template <typename TQI>
QueueItem<TQI>::QueueItem()
	: next_(nullptr)
{}
template <typename TQI>
QueueItem<TQI>::QueueItem(const QueueItem<TQI>& n)
	: item_(n.item_), next_(n.next_)
{}
template <typename TQI>
QueueItem<TQI>::QueueItem(const TQI& item)
	: item_(item), next_(nullptr)
{}
//-----------------END OF QUEUEITEM STUFF-------------


template<typename T>
class Queue
{
public:
	Queue();
	~Queue();
	T first();
	void removeFirst();
	int length() const;
	void addToFront(T);
	void addToEnd(T);
	T getObjAtPos(int) const;
	bool deleteItem(T);
	bool updateItem(T); //Used to update an item that already exists in the list, primarily used for nodeScores
private:
	QueueItem<T>* head;
	//QueueItem<T>* end;
};

//for unknown reasons, the compiler gives a linker error when this section of code is moved into the .cpp, I would fix it, but its 4 a.m - Callum
template<typename T>
Queue<T>::Queue()
{
	head = nullptr;
	//end = nullptr;
}
template<typename T>
Queue<T>::~Queue()
{
	//release all queueItems
	QueueItem<T>* pos = head;
	while (pos != nullptr)
	{
		QueueItem<T>* temp = pos;
		pos = pos->next_;
		delete(temp);
	}
}
template<typename T>
T Queue<T>::first()
{
	return(head->item_);
}
template<typename T>
void Queue<T>::removeFirst()
{
	QueueItem<T>* temp = head;
	head = head->next_;//move to the next item
	delete(temp);
}

template<typename T>
int Queue<T>::length() const
{
	QueueItem<T>* pos = head;
	int count = 0;
	while (pos != nullptr)
	{
		pos = pos->next_;
		count++;
	}

	return count;
}

template<typename T>
void Queue<T>::addToFront(T item)
{
	QueueItem<T>* temp(new QueueItem<T>(item));
	temp->next_ = head;
	head = temp;
}

template<typename T>
void Queue<T>::addToEnd(T item)
{
	QueueItem<T>* pos = head;
	if (pos != nullptr) {
		while (pos->next_ != nullptr)
		{
			pos = pos->next_;
		}
		QueueItem<T>* temp(new QueueItem<T>(item));
		pos->next_ = temp;
	}
	else
	{
		QueueItem<T>* temp(new QueueItem<T>(item));
		head = temp;
	}
}

template<typename T>
bool Queue<T>::updateItem(T item)
{
	QueueItem<T>* pos = head;
	bool found = false;
	if (pos != nullptr)
	{
		if (item == pos->item_)
		{
			found = true;
			QueueItem<T>* temp(new QueueItem<T>(item));
			temp->next_ = pos->next_;
			delete(pos);
			head = temp;
		}
		else
		{
			while (pos->next_ != nullptr)
			{
				if (item == pos->next_->item_) //I know this looks incredibly stupid, but this works for things like NodeScores, where I made a custom operator, so I can update the score of a specific node
				{
					found = true;
					QueueItem<T>* temp(new QueueItem<T>(item));
					temp->next_ = pos->next_->next_;
					delete(pos->next_);
					pos->next_ = temp;
				}
				pos = pos->next_;
			}
		}
	}

	return(found);
}

template<typename T>
T Queue<T>::getObjAtPos(int i) const
{
	QueueItem<T>* pos = head;
	int count = 0;
	while (pos != nullptr && count < i)
	{
		pos = pos->next_;
		count++;
	}

	return pos->item_;
}

template<typename T>
bool Queue<T>::deleteItem(T item)
{
	QueueItem<T>* pos = head;
	bool found = false;
	if (pos != nullptr)
	{
		if (item == pos->item_)
		{
			found = true;
			head = pos->next_;
			delete(pos);
		}
		else
		{
			while (pos->next_ != nullptr)
			{
				if (item == pos->next_->item_) //I know this looks incredibly stupid, but this works for things like NodeScores, where I made a custom operator, so I can update the score of a specific node
				{
					found = true;
					QueueItem<T>* temp = pos->next_->next_;
					delete(pos->next_);
					pos->next_ = temp;
				}
				else
					pos = pos->next_;
			}
			
		}
	}

	return(found);
}


#endif
