#include "ControlsMode.h"
#include "GameModeManager.h"






void ControlsMode::Update(float dTime)
{
	menu.Update(dTime);
	if (menu.isButtonPressed(0))
	{
		GameModeManager::GetInstance()->SwapMode(0);
	}

	GetGamepad()->Update();
}

void ControlsMode::Render(float dTime)
{
	BeginRender(Colours::Black);

	FX::MyFX& fx = *FX::GetMyFX();

	menu.Render(dTime);

	fx.mpSpriteB->Begin(SpriteSortMode_Deferred);

	//background
	int w, h;
	GetClientExtents(w, h);
	std::string text = "Controls\nWASD / Gamepad Left stick: Movement\nQ / E / Gamepad L / R Bumpers : Swap weapons\nLeft click / Right Trigger : Fire\nESC / Gamepad Start : Pause";
	//wchar_t* displayText = new wchar_t[text.size()/2];
	//std::copy(text.begin(), text.end(), displayText);
	std::wstring displayText(text.begin(), text.end());

	Vector2 origin = fx.mpFont->MeasureString(displayText.c_str()) / 2.f;
	fx.mpFont->DrawString(fx.mpSpriteB, displayText.c_str(), Vector2(w / 2, h / 2), Colors::Aqua, 0.f, origin);
	fx.mpSpriteB->End();

	EndRender();
}

void ControlsMode::KeyPressEvent()
{
}
