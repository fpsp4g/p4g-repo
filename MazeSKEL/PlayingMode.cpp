#include "PlayingMode.h"
#include "GameModeManager.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;


PlayingGameMode::PlayingGameMode(LevelManager* levelManagerr)
{
	levelManager = levelManagerr;
}


PlayingGameMode::~PlayingGameMode()
{
	delete GetMeshManager();
}


void PlayingGameMode::Initialise()
{
	//spriteSheetTexture = FX::GetMyFX()->mCache.LoadTexture("data/spritesheet.dds", false, gd3dDevice);

	//LIAM doing this once we've already loaded the levelmanager before will cause errors when it tries to create a vertex buffer for the level
	//levelManager.Init(spriteSheetTexture);
	new RequestHandler;
	//new EnemyManager;
	//EnemyManager::GetInstance()->SetLevel(levelManager.GetCurrentLevel());

	//music
	GetIAudioMgr()->GetSongMgr()->Play("TheArenaP4A", true, false, &musicHdl, 0.01);

	Mesh* sb = MeshManager::Get()->GetMesh("skybox");// <-- This will most likely crash when loaded again beware! - Tom
	mSkybox.Initialise(*sb);
	mSkybox.GetScale() = Vector3(1, 1, 1);
	mSkybox.GetPosition() = Vector3(0, 0, 0);
	mSkybox.GetRotation() = Vector3(PI / 2, 0, 0);
	MaterialExt& defMat = mSkybox.GetMesh().GetSubMesh(0).material;
	defMat.flags &= ~MaterialExt::LIT;
	defMat.flags &= ~MaterialExt::ZTEST;
	
	Mesh* m1 = GetMeshManager()->GetMesh("PABHC_Payload");

	Mesh* m2 = GetMeshManager()->GetMesh("Enemy_Projectile");
	
	//EnemyManager::GetInstance()->SpawnEnemy(Vector3(4.5, 0, 2.5));
	//EnemyManager::GetInstance()->SpawnEnemy(Vector3(1.5, 0, 1.5));
	FX::SetupDirectionalLight(0, true, Vector3(-0.7f, -0.7f, 0.7f), Vector3(0.67f, 0.67f, 0.67f), Vector3(0.25f, 0.25f, 0.25f), Vector3(0.0f, 0.0f, 0.0f));

	CreateProjectionMatrix(FX::GetProjectionMatrix(), 0.25f*PI, GetAspectRatio(), 0.1f, 1000.f);
	testM.Initialise(BuildCube(*MeshManager::Get()));

	GetGamepad()->Update();
}

void PlayingGameMode::Release()
{
	Leaderboard::GetInstance()->SaveScore();
	RequestHandler::GetInstance()->release();
	//EnemyManager::GetInstance()->Release();
	delete levelManager;
	levelManager = nullptr;
}

void PlayingGameMode::Update(float dTime)
{
	RequestHandler::GetInstance()->ProcessNextRequest();

	//EnemyManager::GetInstance()->UpdateEnemies(dTime);

	pauseMenu.Update(dTime);
	if (levelManager->GetPlayer()->isPaused())
	{
		if (pauseMenu.isPlaying() == true)
			pauseMenu.SetPlaying(false);

		if (pauseMenu.isButtonPressed(0)) // Continue
		{
			levelManager->GetPlayer()->TogglePause();
		}
		if (pauseMenu.isButtonPressed(1)) // Exit button
		{
			GetIAudioMgr()->GetSongMgr()->Stop();
			GameModeManager::GetInstance()->SwapMode(0);
		}
	}
	else
	{
		if (pauseMenu.isPlaying() == false)
			pauseMenu.SetPlaying(true);
	}

	GetGamepad()->Update();
}

void PlayingGameMode::Render(float dTime) 
{
	BeginRender(Colours::Black);
	
	mSkybox.GetPosition() = FX::GetMyFX()->cameraPos;
	FX::GetMyFX()->Render(mSkybox, gd3dImmediateContext);
	//FX::MyFX& fx = *FX::GetMyFX();

	//CreateViewMatrix(FX::GetViewMatrix(), mCamPos, Vector3(0, 0, 0), Vector3(0, 1, 0));

	levelManager->Render(dTime);
	FX::GetMyFX()->Render(testM, gd3dImmediateContext);

	//EnemyManager::GetInstance()->RenderEnemies(dTime);
	levelManager->GetPlayer()->Render(dTime);

	if(levelManager->GetPlayer()->isPaused())
		pauseMenu.Render(dTime);

	EndRender();

	GetMouseAndKeys()->PostProcess();
}

void PlayingGameMode::KeyPressEvent()
{

}

