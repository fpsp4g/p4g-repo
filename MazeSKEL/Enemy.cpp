#include "Enemy.h"
#include "Leaderboard.h"


Enemy::Enemy(string modelName, int hp, Vector3 position, Vector3 scale, Vector3 rotation, float speed)
	:Character(hp), spawnPos(position), currentNode(0,0,false), moveSpeed(speed)
{
	SetPosition(position);
	string filePath = "data/enemies/" + modelName + ".fbx";
	InitialiseModel(model,modelName, filePath, scale, position, rotation);
}

Enemy::~Enemy(){}


void Enemy::PathTo(Node endNode)
{
	
	pathFound = false;
	RequestHandler::GetInstance()->AddRequestToQueue((level->GetBlockAt(position.x, position.z)->GetNode()), endNode, &this->currentPath, &pathFound);
}

void Enemy::MoveTowards(Vector3 target, float dTime) 
{
	Vector3 nodePos(target.x, GetPosition().y, target.z);
	Vector3 temp = (nodePos - GetPosition());

	temp.Normalize();
	temp = GetPosition() + (temp * dTime * moveSpeed);

	if (level->GetBlockAt(temp.x, temp.z)->GetNode().getWalkable()) //make sure that the enemy stays within the bounds of the map
	{
		SetPos(temp);
	}
}

void Enemy::Die()
{
	//Do enemy related death stuff
	//do death effect, maybe need to update enemy manager
	//dead = true;
	if (GetIAudioMgr()->GetSfxMgr()->IsPlaying(deathSound) == false)
		GetIAudioMgr()->GetSfxMgr()->Play("145788__cyberios__small-explosion", false, false, &deathSound, 0.5 / (level->GetPlayer()->GetPosition() - position).Length());
	else
		GetIAudioMgr()->GetSfxMgr()->Stop(deathSound);
	Leaderboard::GetInstance()->GetCurrentScore()->ChangeScore(1);
	DestroySelf();
}


void Enemy::Render(float dTime)
{
	if(visible)
		FX::GetMyFX()->Render(model, gd3dImmediateContext);
}

void Enemy::SetPos(Vector3 pos)
{
	position = pos;
	model.GetPosition() = pos;
}

bool Enemy::CanSeeTarget(Node target) {
	if (canSendLineRequest) {
		canSendLineRequest = false;
		RequestHandler::GetInstance()->DrawLine(level->GetBlockAt(GetPosition().x, GetPosition().z)->GetNode(), target, &canSendLineRequest, &canSeePlayer);
	}
		
	return canSeePlayer;
}

void Enemy::Update(float dTime) {

	if (!level || !level->PlayerExists()) {
		return; //Level or player has not been instantiated yet so dont bother
	}
	RotateToFacePlayer();
	switch (state)
	{
	case IDLE:
		IdleBehaviour(dTime);
		break;
	case PATROL:
		PatrolBehaviour(dTime);
		break;
	case CHASE:	
		ChaseBahaviour(dTime);
		break;
	case SEARCH:
		SearchBehaviour(dTime);
		break;
	case ATTACK:
		AttackBehaviour(dTime);
		break;
	default:
		break;
	}
}

void Enemy::Collision(Entity* entityCollidedWith, bool inXAxis, bool inZAxis)
{
	if (entityCollidedWith == level->GetPlayer()) {
		level->GetPlayer()->TakeDamage(10);
	}
}

DirectX::BoundingBox Enemy::GetBoundingBox()
{
	return DirectX::BoundingBox(position, Vector3(0.4f, 1.0f, 0.4f));
}

void Enemy::RotateToFacePlayer()
{
	bool isOnLeft = false;
	if (position.x > level->GetPlayer()->GetPosition().x)
		isOnLeft = true;
	Vector3 vecToPlayer = position - level->GetPlayer()->GetPosition();
	vecToPlayer.Normalize();
	
	float y = acos(Vector3(0, 0, 1).Dot(vecToPlayer));
	
	if (isOnLeft)
	{
		model.GetRotation() = Vector3(model.GetRotation().x, y, model.GetRotation().z);
		forward = Vector3::TransformNormal(Vector3(0, 0, -1), Matrix::CreateRotationY(y));
	}
	else
	{
		model.GetRotation() = Vector3(model.GetRotation().x, -y, model.GetRotation().z);
		forward = Vector3::TransformNormal(Vector3(0, 0, -1), Matrix::CreateRotationY(-y));
	}
}
