#include "Node.h"

Node::Node(int x, int y, bool nonWalk):x(x), y(y), nonWalkable(nonWalk)
{

}

Node::~Node()
{
}

void Node::FindNodes(std::vector<Node*>* map, int width, int height) {
	//currently just adds nodes to directly adjacent to the current node, diagonals can be added if wanted, however
	bool xGreaterThanZero(false), xSmallerThanMap(false);
	if (!nonWalkable) {
		if (x < height) {
			xSmallerThanMap = true;
			if (!map->at((y * (width)) + x + 1)->nonWalkable)
				linkedNodes.push_back(NodeScore<Node>(map->at((y * (width)) + x + 1), 1, *this));
		}
		if (x > 0) {
			xGreaterThanZero = true;
			if (!map->at((y * (width)) + x - 1)->nonWalkable)
				linkedNodes.push_back(NodeScore<Node>(map->at((y * (width)) + x - 1), 1, *this));
		}
		if (y < width) {
			if (xSmallerThanMap) {
				if (!map->at(((y + 1) * (width)) + x + 1)->nonWalkable)
					linkedDiagonals.push_back(map->at(((y + 1) * (width)) + x + 1));
			}
			if (xGreaterThanZero) {
				if (!map->at(((y + 1) * (width)) + x - 1)->nonWalkable)
					linkedDiagonals.push_back(map->at(((y + 1) * (width)) + x - 1));
			}
			if (!map->at((y + 1) * (width)+x)->nonWalkable)
				linkedNodes.push_back(NodeScore<Node>(map->at((y + 1) * (width)+x), 1, *this));
		}
		if (y > 0) {
			if (xSmallerThanMap) {
				if (!map->at(((y - 1) * (width)) + x + 1)->nonWalkable)
					linkedDiagonals.push_back(map->at(((y - 1) * (width)) + x + 1));
			}
			if (xGreaterThanZero) {
				if (!map->at(((y - 1) * (width)) + x - 1)->nonWalkable)
					linkedDiagonals.push_back(map->at(((y - 1) * (width)) + x - 1));
			}
			if (!map->at((y - 1) * (width)+x)->nonWalkable)
				linkedNodes.push_back(NodeScore<Node>(map->at((y - 1) * (width)+x), 1, *this));
		}
	}
}

void Node::initialise(int x, int y) {
	if (this->x == 0 && this->y == 0)
	{
		this->x = x;
		this->y = y;
	}
}

void Node::setNonWalkable(bool nonWalk) {
	nonWalkable = nonWalk;
}
const int Node::LengthOfLinkedNodes() const
{
	return linkedNodes.size();
}
const std::vector<NodeScore<Node>> Node::GetLinkedNodes() const
{
	return linkedNodes;
}

bool Node::operator==(const Node& rhs) {
	return!(*this != rhs);
}

bool Node::operator!=(const Node& rhs) {
	bool notEqual = false;
	//if(this->linkedNodes.size() != rhs.linkedNodes.size())
		//notEqual = true;
	//else 
	if (this->x != rhs.x || this->y != rhs.y)
		notEqual = true;
	else if (this->nonWalkable != rhs.nonWalkable)
		notEqual = true;
	//else
	//{
		//for (int i = 0; i < this->linkedNodes.size(); i++)
		//{
			//if (this->linkedNodes[i].node->x != rhs.linkedNodes[i].node->x && this->linkedNodes[i].node->y != rhs.linkedNodes[i].node->y)
				//notEqual = true;
		//}
	//}
	return notEqual;
}

float Node::getDistanceToNode(Node newNode)
{
	int test = pow((pow(x - newNode.x, 2) + pow(y - newNode.y, 2)), 0.5);
	return test;
}