#ifndef BOSS_H
#define BOSS_H

#include "RangedEnemy.h"
#include "Tunneller.h"

class Boss : public RangedEnemy
{
public:
	Boss(Vector3 pos);
	virtual ~Boss();
	virtual void ChaseBahaviour(float);
protected:
	virtual void Die();
private:
	const float maxTunnellerTimer = 5.0f;
	float tunnellerTimer = maxTunnellerTimer;
	float attackTimer = 0;
	const float maxAttackTime = 1.5f;	//base value for the delay between attacks, can be changed at will, and it may be worth changing it on a per enemy basis
};

#endif // !

