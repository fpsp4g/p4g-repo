/*******************************************************
* Copyright (C) 2018 Thomas Dunn tom1998dunn@hotmail.com
*
* This file is part of P4G Assignment.
*
* This code can not be copied and/or distributed without the express
* permission of Thomas Dunn
*******************************************************/

#ifndef BLOCKH
#define BLOCKH

#include <vector>
#include "Node.h"
#include "Entity.h"



class Block {

public:
	Block();
	Block(bool hasCol);
	virtual ~Block();

	virtual void Update(float dTime);

	long GetCeilingColour() const;
	long GetFloorColour() const;
	long GetWallColour() const;

	int GetCeilingTextureID() const;
	int GetFloorTextureID() const;
	int GetWallTextureID() const;
	void SetID(int id);
	int GetID() const;
	virtual void Trigger(bool pressed);

	bool HasCollision() const { return hasCollision; }
	virtual void OnTrigger() {}	// Used later

	std::vector<Entity*> entities;				// Entities currently on the block (When a entity walks on a block) <- Used for collision
	std::vector<Entity*> sprites;				// Entities currently attached to the block (Aka has to be rendered with block)

	//CALLUM - Added the following functions
	const Node GetNode();
	Node* GetNodePointer();
	void InitialiseNode(int x, int y);
	void LinkBlockToMap(class Level* level, std::vector<Node*>* mapNode, unsigned width, unsigned height);
	
protected:
	bool hasCollision;

	Node node;		// CALLUM - Added node

	unsigned m_width, m_height; // Map width + height
	class Level* level;

	int id;		// Use of alpha code

	long ceilingColour;
	long floorColour;
	long wallColour;

	int floorTextureID;
	int ceilingTextureID;
	int wallTextureID;
};

#endif