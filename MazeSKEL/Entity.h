#ifndef ENTITYH
#define ENTITYH

#include "D3D.h"
#include "D3DUtil.h"
#include <vector>
#include "Model.h"
#include "LevelPointLightManager.h"

class Entity
{
public:
	Entity() :map(nullptr),requestDeleteSelf(false), isTriggerCollision(false), hasLevelCollision(true){}
	virtual ~Entity() {}
	virtual void HardUpdate(float dTime);			// Handles the whole updating system 
	virtual void OnEnable();
	virtual void OnDisable();
	virtual void Render(float dTime) = 0;
	virtual void Update(float dTime) = 0;
	virtual void Trigger(bool pressed);
	void HandleLevelCollision();
	void HandleEntityCollision();
	float GetDistanceFromPlayer() const;

	// Handles what happens when collided
	virtual void Collision(Entity* entityCollidedWith, bool inXAxis, bool inZAxis);
	// Handles what happens when collided on level
	virtual void OnLevelCollision(bool inXAxis, bool inZAxis);

	void SetPosition(const DirectX::SimpleMath::Vector3& position_) { position = position_; }
	void Reset(const DirectX::SimpleMath::Vector3& position_);
	const DirectX::SimpleMath::Vector3 GetPosition() const { return position; }

	bool HasRequestedDelete()const { return requestDeleteSelf; }
	void DestroySelf() { requestDeleteSelf = true; }		// Used to destroy object
	void LinkEntityToMap(class Level* level);
	void UnlinkEntityFromMap();

protected:
	void InitialiseModel(Model& model,const std::string& meshName, const std::string& filePath, const DirectX::SimpleMath::Vector3& scale,const DirectX::SimpleMath::Vector3& position, const DirectX::SimpleMath::Vector3& rotation);
	DirectX::SimpleMath::Vector3 position,lastPosition;

	unsigned m_width, m_height; // Map width + height

	std::vector<class Block*>* map;

	class Level* level = nullptr;

	bool isTriggerCollision;			// Whether you can walk through this object or not
	bool hasLevelCollision;				// Whether you can walk through walls

	virtual DirectX::BoundingBox GetBoundingBox();



private:
	bool requestDeleteSelf;			// Used to see if object has been destoryed

};
#endif