#ifndef DYNAMICDOORH
#define DYNAMICDOORH

#include "DecalEntity.h"

class DynamicDoor : public DecalEntity {

public:
	DynamicDoor(float x, float z, float yRotation);
	virtual void Update(float dTime);

protected:
	void UpdateAnimation(float dTime);
	bool isOpening;
private:
	unsigned int swooshSound;
	const float DISTANCE_TO_OPEN;
	const int MIN_TEXTURE,MAX_TEXTURE;
	float time;
	
};

#endif