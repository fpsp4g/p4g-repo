#include "DecalRenderer.h"
#include "DecalEntity.h"

void DecalRenderer::Init(ID3D11ShaderResourceView* spriteSheetTexture, TextureRegionHandler* textureRegionHandler)
{
	this->textureRegionHandler = textureRegionHandler;

	// Allocate memory in vertex and index buffer
	vertices.insert(vertices.begin(), MAX_VERTICES, VertexPosNormTexColor());
	CreateVertexBuffer(sizeof(VertexPosNormTexColor) * MAX_VERTICES, &vertices[0], vertexBuffer, true);
	indices.insert(indices.begin(), MAX_VERTICES * 3, 0);
	CreateIndexBuffer(sizeof(UINT)*(MAX_VERTICES * 3), &indices[0], indexBuffer, true);
	//---------------------------------------------

	// Sets the texture to use our sprite sheet texture
	material = MaterialExt::default;
	material.pTextureRV = spriteSheetTexture;

	// Clear memory
	vertices.clear();
	indices.clear();
}

void DecalRenderer::Render(const Vector3 & position, const Vector3 & scale, const Vector3 & rotation, const Vector3& tint, int textureIndex)
{
	int size = vertices.size();
	// Picks the right vertices to render this quad
	indices.push_back(size + 0);
	indices.push_back(size + 1);
	indices.push_back(size + 2);

	indices.push_back(size + 2);
	indices.push_back(size + 3);
	indices.push_back(size + 0);
	//-------------------------
	numIndices = indices.size();

	VertexPosNormTexColor v1, v2, v3, v4;
	DirectX::SimpleMath::Vector3 d_pos = position;
	const DirectX::SimpleMath::Vector3& d_rot = rotation;
	const DirectX::SimpleMath::Vector3& d_scale = scale;
	const DirectX::SimpleMath::Vector3& d_scale_half = d_scale / 2.0f;
	// Rotation matrix
	DirectX::SimpleMath::Matrix rotMatrix = DirectX::SimpleMath::Matrix::CreateRotationY(DirectX::XMConvertToRadians(-d_rot.y));
	rotMatrix *= DirectX::SimpleMath::Matrix::CreateRotationZ(DirectX::XMConvertToRadians(-d_rot.z));
	rotMatrix *= DirectX::SimpleMath::Matrix::CreateRotationX(DirectX::XMConvertToRadians(-d_rot.x));
	//// Does rotation

	v1.Pos = DirectX::XMVector3Transform(DirectX::SimpleMath::Vector3(0 - d_scale_half.x, 0 - d_scale_half.y, 0), rotMatrix);
	v2.Pos = DirectX::XMVector3Transform(DirectX::SimpleMath::Vector3(0 - d_scale_half.x, d_scale.y - d_scale_half.y, 0), rotMatrix);
	v3.Pos = DirectX::XMVector3Transform(DirectX::SimpleMath::Vector3(d_scale.x - d_scale_half.x, d_scale.y - d_scale_half.y, 0), rotMatrix);
	v4.Pos = DirectX::XMVector3Transform(DirectX::SimpleMath::Vector3(d_scale.x - d_scale_half.x, 0 - d_scale_half.y, 0), rotMatrix);

	// Puts object in correct place
	v1.Pos += DirectX::SimpleMath::Vector3(d_pos.x, d_pos.y + 0.5f, d_pos.z);
	v2.Pos += DirectX::SimpleMath::Vector3(d_pos.x, d_pos.y + 0.5f, d_pos.z);
	v3.Pos += DirectX::SimpleMath::Vector3(d_pos.x, d_pos.y + 0.5f, d_pos.z);
	v4.Pos += DirectX::SimpleMath::Vector3(d_pos.x, d_pos.y + 0.5f, d_pos.z);

	v1.Norm = DirectX::SimpleMath::Vector3(0, 1, 0);
	v2.Norm = DirectX::SimpleMath::Vector3(0, 1, 0);
	v3.Norm = DirectX::SimpleMath::Vector3(0, 1, 0);
	v4.Norm = DirectX::SimpleMath::Vector3(0, 1, 0);

	v1.Color = tint;
	v2.Color = tint;
	v3.Color = tint;
	v4.Color = tint;

	const TextureRegion& tc = textureRegionHandler->GetTexture(textureIndex);

	v1.Tex = DirectX::SimpleMath::Vector2(tc.GetMinU(), tc.GetMinV() + tc.GetMaxV());
	v2.Tex = DirectX::SimpleMath::Vector2(tc.GetMinU(), tc.GetMinV());
	v3.Tex = DirectX::SimpleMath::Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV());
	v4.Tex = DirectX::SimpleMath::Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV() + tc.GetMaxV());

	vertices.push_back(v1);
	vertices.push_back(v2);
	vertices.push_back(v3);
	vertices.push_back(v4);

	// Flush if it gets to big
	if (vertices.size() >= MAX_VERTICES)
		Flush();
}

void DecalRenderer::Release()
{
	if (vertexBuffer)
		vertexBuffer->Release();
	if (indexBuffer)
		indexBuffer->Release();
}

void DecalRenderer::Render(const DecalEntity* decal)
{
	DecalRenderer::Render(decal->GetPosition(), decal->GetScale(), decal->GetRotation(), decal->GetTint(), decal->GetTextureIndex());
}

void DecalRenderer::Flush()
{
	int numIndices = indices.size();
	if (numIndices > 0) {
		// Updates the mesh data
		UpdateDynamicBuffer(indexBuffer, [&](void*pDest) {
			memcpy(pDest, &indices[0], sizeof(UINT)*numIndices);
		});
		int numVertices = vertices.size();
		UpdateDynamicBuffer(vertexBuffer, [&](void*pDest) {
			memcpy(pDest, &vertices[0], sizeof(VertexPosNormTexColor) * numVertices);
		});

		////setup shaders
		gd3dImmediateContext->VSSetShader(FX::GetMyFX()->mpTomVS, nullptr, 0);						// Enables this vertex shader
		gd3dImmediateContext->PSSetShader(FX::GetMyFX()->mpTomPS, nullptr, 0);						// Enables this pixel shader

		DirectX::SimpleMath::Matrix w = DirectX::SimpleMath::Matrix::CreateTranslation(DirectX::SimpleMath::Vector3(0, 0, 0));																// Set a very simple matrix for use in shader
		FX::SetPerObjConsts(gd3dImmediateContext, w);																		// Magic marks code here, I believe it just passes uniforms to the vertex and pixel shader
		InitInputAssembler(FX::GetMyFX()->mpTomInputLayout, vertexBuffer, sizeof(VertexPosNormTexColor), indexBuffer);		// More magic
		FX::GetMyFX()->PreRenderObj(gd3dImmediateContext, material, true);													// Sets up the shader to use the material
		gd3dImmediateContext->DrawIndexed(numIndices, 0, 0);																// Draws onto the screen

		gd3dImmediateContext->OMSetBlendState(0, 0, 0xffffffff);
		//-------------- After rendering
		vertices.clear();
		indices.clear();
	}
}
