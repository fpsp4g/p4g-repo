#pragma once
#include "Menu.h"
class ControlsMenu :
	public Menu
{
public:
	ControlsMenu();
	~ControlsMenu();
	virtual void RenderBackground(int w, int h);
	virtual void RenderTitle(int w, int h);
	virtual void RenderButtons(int w, int h);

private:
	virtual void InitialiseButtons();
};

