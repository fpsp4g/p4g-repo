#ifndef MoveableWorldObjectH
#define MoveableWorldObjectH

#include "WorldObject.h"

class MoveableWorldObject :
	public WorldObject
{
public:
	MoveableWorldObject();
	~MoveableWorldObject();
};
#endif