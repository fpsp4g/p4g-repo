#ifndef PARTICLEH
#define PARTICLEH

#include "BillboardEntity.h"

class Particle : public BillboardEntity {

public:
	Particle(DirectX::SimpleMath::Vector3 position, DirectX::SimpleMath::Vector3 velocity,float timeTillDestoryInSecs);
	virtual void Update(float dTime);

private:
	DirectX::SimpleMath::Vector3 velocity;
	float time{ 0 };
	const float destroyTimeInSeconds;

};

#endif