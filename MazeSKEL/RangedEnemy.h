#ifndef RANGEDENEMY_H
#define RANGEDENEMY_H

#include "Enemy.h"
#include "EnemyProjectile.h"
class RangedEnemy : public Enemy
{
public:
	RangedEnemy(Vector3 pos, string model = "TurretDrone", int hp = 100);
	virtual ~RangedEnemy();
	virtual void SearchBehaviour(float);
	virtual void PatrolBehaviour(float);
	virtual void IdleBehaviour(float);
	virtual void AttackBehaviour(float);
	virtual void ChaseBahaviour(float);
protected:
	virtual void Die();
private:
	float attackTimer = 0;
	const float maxAttackTime = 1.5f;	//base value for the delay between attacks, can be changed at will, and it may be worth changing it on a per enemy basis
};

#endif // !

