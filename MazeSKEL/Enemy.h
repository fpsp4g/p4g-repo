#ifndef EnemyH
#define EnemyH

#include "Character.h"
#include "RequestHandler.h"
#include "Queue.h"
#include "EnemyState.h"
#include "AudioMgr.h"
#include "AudioMgrFMOD.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

class Enemy :
	public Character
{
public:
	Enemy(string, int, Vector3, Vector3, Vector3, float);
	virtual ~Enemy();
	void PathTo(Node endNode); //Ask the request handler to get us a new path to a new end node
	virtual void Update(float dTime);
	void Render(float dTime);
	void SetPos(Vector3 position);
	bool IsDead() { return dead; }
	virtual void SearchBehaviour(float) = 0;
	virtual void PatrolBehaviour(float) = 0;
	virtual void IdleBehaviour(float) = 0;
	virtual void AttackBehaviour(float) = 0;
	virtual void ChaseBahaviour(float) = 0;
	virtual void Collision(Entity* entityCollidedWith, bool inXAxis, bool inZAxis);
	virtual DirectX::BoundingBox GetBoundingBox();
private:
	
	Model model;
	bool dead = false;
	void RotateToFacePlayer();
	bool canSendLineRequest = true;
protected:
	unsigned int deathSound;
	bool canSeePlayer = false;
	float GetYRotation() { return model.GetRotation().y; }
	void MoveTowards(Vector3, float);
	virtual void Die(); 
	Vector3 forward = Vector3(0, 0, -1);
	bool CanSeeTarget(Node Target);
	Node currentNode; //Node we are currently on
	Queue<Node> currentPath; //Our current pathfinding path
	bool pathFound = false;
	bool pathRequested = false;
	float moveSpeed;
	EnemyState state = EnemyState::IDLE;
	Vector3 spawnPos;
	unsigned int shotSound;
};
#endif