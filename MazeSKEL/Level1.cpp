#include "Level1.h"
#include "LevelManager.h"

void Level2::Init() {
	floorTexture = 33;
	floorColour = 0x00bb00;
	ceilingTexture = -1;
	wallTexture = 2;
	wallColour = 0x00bb00;

	CreateMap("data/levels/level1.png");
}

void Level2::SwitchLevel(int id)
{
	if(id == 1) LevelManager::GetMyLevelManager()->SwitchLevel("TestLevel",2);
}
