#include "EnemyProjectile.h"
#include "Level.h"
#include "Player.h"

EnemyProjectile::EnemyProjectile(Vector3 pos, Vector3 rotation, Vector3 dir)
	: Projectile
	(
		"Enemy_Projectile",					// Model name
		pos,								// Projectile start position
		Vector3(0.001f, 0.001f, 0.001f),	// Model scale
		rotation,							// Model rotation
		dir,								// Projectile movement direction
		5,									// Projectile damage
		10,									// Projectile speed
		0,									// Projectile lifetime
		true								// Is the projectile visible?
	)
{ }

EnemyProjectile::~EnemyProjectile()
{ }

void EnemyProjectile::Update(float dTime)
{
	position += direction * speed * dTime;
	SetModelPosition(position);
}

DirectX::BoundingBox EnemyProjectile::GetBoundingBox()
{
	return DirectX::BoundingBox(position, Vector3(0.3f, 1.0f, 0.3f));
}

void EnemyProjectile::OnLevelCollision(bool inXAxis, bool inZAxis)
{
	if (inXAxis || inZAxis)
	{
		if (GetIAudioMgr()->GetSfxMgr()->IsPlaying(wallHitSound) == false)
			GetIAudioMgr()->GetSfxMgr()->Play("150837__toxicwafflezz__bullet-impact-1", false, false, &wallHitSound, 0.5 / (level->GetPlayer()->GetPosition() - position).Length());
		else
			GetIAudioMgr()->GetSfxMgr()->Stop(wallHitSound);

		DestroySelf();
	}
}

void EnemyProjectile::Collision(Entity* entityCollidedWith, bool inXAxis, bool inZAxis)
{
	Player* temp = dynamic_cast<Player*>(entityCollidedWith);
	if (temp)
	{
		temp->TakeDamage(GetDamage());
		DestroySelf();
	}
}
