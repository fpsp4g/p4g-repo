#ifndef LeaderboardModeH
#define LeaderboardModeH
#include "GameMode.h"
#include "CommonStates.h"
#include <algorithm>
#include <vector>
#include "Mesh.h"
#include "Model.h"
#include "FPSCamera.h"
#include "WindowUtils.h"
#include "D3D.h"
#include "GeometryBuilder.h"
#include "FX.h"
#include "Input.h"
#include "LevelManager.h"
#include "Camera.h"
#include "Leaderboard.h"
#include <string>
#include <cstring>

#include "LeaderboardMenu.h"

class LeaderboardMode :
	public GameMode
{
public:
	LeaderboardMode();
	~LeaderboardMode();
	 void Initialise() ;
	 void Update(float) ;
	 void Render(float) ;
	 void Release() ;
	 void KeyPressEvent() ;

private:
	ID3D11ShaderResourceView * mpLeadFont = nullptr;
	DirectX::SimpleMath::Vector2 mLeadDim;
	Camera mCamera;
	std::vector<Score> scoresToShow;

	LeaderboardMenu ldrbrdMenu;

};
#endif