#ifndef MELEEENEMY_H
#define MELEEENEMY_H

#include "Enemy.h"

class MeleeEnemy : public Enemy
{
public:
	MeleeEnemy(Vector3 pos);
	virtual ~MeleeEnemy();
	virtual void SearchBehaviour(float);
	virtual void PatrolBehaviour(float);
	virtual void IdleBehaviour(float);
	virtual void AttackBehaviour(float);
	virtual void ChaseBahaviour(float);
	virtual void Collision(Entity* entityCollidedWith, bool inXAxis, bool inZAxis);
protected:
	virtual void Die();
private:

};






#endif // !

