#ifndef TURRETENEMY_H
#define TURRETENEMY_H

#include "Enemy.h"
#include "EnemyProjectile.h"
class TurretEnemy : public Enemy
{
public:
	TurretEnemy(Vector3);
	virtual ~TurretEnemy();
	virtual void SearchBehaviour(float);
	virtual void PatrolBehaviour(float);
	virtual void IdleBehaviour(float);
	virtual void AttackBehaviour(float);
	virtual void ChaseBahaviour(float);
protected:
	virtual void Die();
private:
	float attackTimer = 1.0f;
	const float maxAttackTime = 0.75f;	//base value for the delay between attacks, can be changed at will, and it may be worth changing it on a per enemy basis
};

#endif // !

