#ifndef GAME_H
#define GAME_H

#include <algorithm>
#include <vector>
#include "Mesh.h"
#include "Model.h"
#include "FPSCamera.h"
#include "WindowUtils.h"
#include "D3D.h"
#include "GeometryBuilder.h"
#include "FX.h"
#include "Input.h"
#include "LevelManager.h"
#include "GameMode.h"


class Game
{
public:
	Game() : currentGameMode(nullptr){}
	~Game() {
		Release();
	}
	void Update(float dTime);
	void Render(float dTime);
	void OnResize(int screenWidth, int screenHeight);
	void Initialise();
	void Release();
	LRESULT WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	Model mSkybox,testM;

	GameMode* currentGameMode;

private:
	Game& operator=(const Game&) = delete;
	Game(const Game& m) = delete;



};

#endif

