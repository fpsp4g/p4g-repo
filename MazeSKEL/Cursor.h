#ifndef CURSOR_H
#define CURSOR_H

#include "FX.h"
#include "D3D.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

class Cursor
{
public:
	Cursor();
	~Cursor();

	void Render(float dTime);

	void UpdatePosition(int x, int y);

	Vector2 GetPosition() { return pos; }

private:
	int cursorTexIdx;
	Vector2 cursorCenter;

	Vector2 pos;

};

#endif
