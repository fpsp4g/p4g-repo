#ifndef NodeH
#define NodeH

#include <vector>

template<typename T>
class NodeScore	//a node and the 'weighting' for that node (the 'cost' of getting to the node and the current node
{
public:
	NodeScore(T*, int, T);
	T* node;
	T prevNode;
	int scoreFromThisNode;
	bool operator==(const NodeScore<T>&);
	bool operator!=(const NodeScore<T>&);
};

template<typename T>
NodeScore<T>::NodeScore(T* pn, int score, T pNodePrev) : node(pn), scoreFromThisNode(score), prevNode(pNodePrev) {}


template<typename T>
bool NodeScore<T>::operator==(const NodeScore<T>& rhs) {
	bool equal = false;
	if (*(rhs.node) == *(this->node)) {
		equal = true;
	}
	return (equal);
}
template<typename T>
bool NodeScore<T>::operator!=(const NodeScore<T>& rhs) {
	bool notEqual = false;
	if (*(rhs.node) != *(this->node)) {
		notEqual = true;
	}
	return notEqual;
}

class Node
{
public:
	Node(int, int, bool);
	~Node();
	void FindNodes(std::vector<Node*>*, int, int);
	const int LengthOfLinkedNodes() const;
	const std::vector<NodeScore<Node>> GetLinkedNodes() const;
	const std::vector<Node*> GetLinkedDiagonals() const { return linkedDiagonals; }
	bool operator==(const Node&);
	bool operator!=(const Node&);
	float getDistanceToNode(Node);
	bool getWalkable() const  { return !nonWalkable; };
	int getX() const { return x; };
	int getY() const { return y; };
	void initialise(int x, int y);
	void setNonWalkable(bool);
private:
	std::vector<NodeScore<Node>> linkedNodes;
	std::vector<Node*> linkedDiagonals; //used for finding a straight line between the enemy and the player
	int x, y;
	bool nonWalkable;
	
};

#endif