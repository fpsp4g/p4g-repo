#ifndef GWDOORH
#define GWDOORH

#include "Door.h"

class GameWonDoor : public Door {
public:
	GameWonDoor(float x, float z, float yRotation);
	virtual void Collision(Entity* entityCollidedWith, bool inXAxis, bool inZAxis);
	bool wait;
};

#endif
