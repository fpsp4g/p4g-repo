#include "Node.h"
#include "Queue.h"
struct PathRequest {
	Node startNode;
	Node endNode;
	Queue<Node>* pathToUpDate;
	bool* pathFound;
};

struct LineRequest {
	Node startNode;
	Node endNode;
	bool* canSearch;
	bool* lineFound;
};