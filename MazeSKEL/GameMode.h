#ifndef GameModeH 
#define GameModeH// <- fix this Liam

#include "AudioMgr.h"
#include "AudioMgrFMOD.h"
class GameMode {
public:
	virtual ~GameMode();		// Needed to add this, you was causing a memory leak - Tom
	virtual void Initialise() = 0;
	virtual void Update(float) = 0;
	virtual void Render(float) = 0;
	virtual void Release() = 0;
	virtual void KeyPressEvent() = 0;
	unsigned int musicHdl;
private:

};
#endif