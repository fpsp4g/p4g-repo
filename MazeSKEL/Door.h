#ifndef DOORH
#define DOORH

#include "DecalEntity.h"
#include "Player.h"

class Door : public DecalEntity {
public:
	Door(float x, float z, float yRotation);
	virtual void Update(float dTime);
	virtual DirectX::BoundingBox GetBoundingBox();
	virtual void Collision(Entity* entityCollidedWith, bool inXAxis, bool inZAxis);
	bool wait;
};

#endif