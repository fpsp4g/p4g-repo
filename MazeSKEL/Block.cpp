#include "Block.h"
#include "Level.h"

Block::Block() : ceilingColour(0xffffff), floorColour(0xffffff), wallColour(0xffffff),hasCollision(false),
wallTextureID(-1),floorTextureID(-1),ceilingTextureID(-1), node(0, 0, true){}

Block::Block(bool hasCol) : ceilingColour(0xffffff), floorColour(0xffffff), wallColour(0xffffff), hasCollision(hasCol),
wallTextureID(-1), floorTextureID(-1), ceilingTextureID(-1), node(0, 0, hasCollision) {}

Block::~Block()
{
	// Clears all entities related to this block
	for (Entity* entity : sprites)	// Only sprites not entities because sprites relate to the block and entities is just saying you're on me but not attached to me
		delete entity;

	sprites.clear();
}

void Block::Trigger(bool pressed) {}

void Block::Update(float dTime)
{
}

long Block::GetCeilingColour() const{return ceilingColour;}		// Ceiling color
long Block::GetFloorColour() const{return floorColour;}			// Floor color
long Block::GetWallColour() const{return wallColour;}			// wall color

int Block::GetCeilingTextureID() const{return ceilingTextureID;}// Ceiling texture
int Block::GetFloorTextureID() const{return floorTextureID;}	// Floor texture
int Block::GetWallTextureID() const{return wallTextureID;}		// Wall texture

void Block::SetID(int id)
{
	this->id = id;
}

int Block::GetID() const
{
	return id;
}


const Node Block::GetNode() {
	return node;
}

Node* Block::GetNodePointer() {
	return &node;
}

void Block::InitialiseNode(int x, int y) {	//work around for not being allowed to initialise nodes in constructor, like you're meant to
	node.initialise(x, y);
}

void Block::LinkBlockToMap(Level* level, std::vector<Node*> *mapNode,unsigned width, unsigned height) {
	this->level = level;
	
	//create a map of nodes, because nodes don't need to know about blocks
	m_width = width;
	m_height = height;

	node.FindNodes(mapNode, width, height);
}
