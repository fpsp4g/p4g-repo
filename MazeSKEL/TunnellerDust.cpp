#include "TunnellerDust.h"
#include "Level.h"

TunnellerDust::TunnellerDust(float x, float z) : ParticleEmitter(x,z)
{
	position = DirectX::SimpleMath::Vector3(x, 0, z);
	isTriggerCollision = true;
}

TunnellerDust::~TunnellerDust()
{
	//auto it = particles.begin();
	//while (it != particles.end())
	//{
	//	(*it)->UnlinkEntityFromMap();
	//	delete (*it);
	//	it++;
	//}
}


void TunnellerDust::Update(float dTime)
{
	if (level != nullptr) {
		time += dTime;
		if (time >= 0.1f) {
			time = 0;

			// Test
			float randomX = (((double)rand() / (RAND_MAX)) * 2) - 1;
			float randomZ = (((double)rand() / (RAND_MAX)) * 2) - 1;
			Particle* p = new Particle(position, DirectX::SimpleMath::Vector3(randomX, 1.0f, randomZ), 0.5f);
			p->SetTint(DirectX::SimpleMath::Vector3(0.357f, 0.09f, 0.071f));
			AddParticle(p);
		}
	}
}
