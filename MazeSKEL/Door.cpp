#include "Door.h"
#include "Level.h"
#include "EnemyManager.h"
Door::Door(float x, float z, float yRotation) : DecalEntity(x, z), wait(true) {
	textureIndex = 102;
	tint = DirectX::SimpleMath::Vector3((1.0f / 255) * 124, (1.0f / 255) * 82, (1.0f / 255) * 53);
	rotation.y = yRotation;
	isTriggerCollision = true;
}


void Door::Update(float dTime)
{
}

DirectX::BoundingBox Door::GetBoundingBox()
{
	return DirectX::BoundingBox(position, DirectX::SimpleMath::Vector3(0.5f, 1.0f, 0.5f));
}

void Door::Collision(Entity * entityCollidedWith, bool inXAxis, bool inZAxis)
{
	if (dynamic_cast<Player*>(entityCollidedWith) && !wait) {
		wait = true;
		// Places the player in the center of the Door - We use this to determin which direction the player is facing
		entityCollidedWith->SetPosition (DirectX::SimpleMath::Vector3(position.x,entityCollidedWith->GetPosition().y,position.z));
		//---------------------------------------------------
		Block* block = level->GetBlockAt(position.x, position.z);
		level->SwitchLevel(block->GetID());
	}

	if (entityCollidedWith == nullptr) {
		wait = false;
	}
}
