#ifndef InventoryH
#define InventoryH

#include <vector>

#include "Item.h"

// Weapons
#include "Weapon.h"

using namespace std;

class Inventory 
{
public:
	Inventory();
	~Inventory();

	void Update(float dTime);

	vector<Weapon*> GetAllWeapons()	{ return weapons; };
	Weapon* GetCurrentWeapon()		{ return currentWeapon; }

	void NextWeapon();
	void PreviousWeapon();

	void AddWeapon(string weaponName);

private:
	void InitialiseWeapons();
	void ReleaseWeapons();

	void SwapWeapons();

	vector<Weapon*> weapons;
	Weapon* currentWeapon;
	int currentWeaponCounter;

	float weaponSwapCooldown = 0.2f;
	float swapDelay = 0;

};


#endif
