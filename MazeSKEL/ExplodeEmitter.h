#ifndef EXPLODEEMITTERH
#define EXPLODEEMITTERH

#include "ParticleEmitter.h"

class ExplodeEmitter : public ParticleEmitter {

public:
	ExplodeEmitter(float x, float z);
	virtual void Update(float dTime);
private:
	float deathTime{ 0.2f };

};

#endif