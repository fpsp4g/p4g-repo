#ifndef MENU_LEADERBOARD_H
#define MENU_LEADERBOARD_H

#include "Menu.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

class LeaderboardMenu : public Menu
{
public:
	LeaderboardMenu();
	virtual ~LeaderboardMenu();

	virtual void RenderBackground(int w, int h);
	virtual void RenderTitle(int w, int h);
	virtual void RenderButtons(int w, int h);

private:
	virtual void InitialiseButtons();

};

#endif