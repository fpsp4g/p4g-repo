#ifndef LeaderboardH
#define LeaderboardH


#include <vector>
#include "Score.h"
#include <string>
#include <cereal\archives\portable_binary.hpp>
#include<cereal\types\string.hpp>
#include <cereal/types/vector.hpp>
#include <fstream>

//On load the Leaderboard should de-serialize from it's save file. On exit the leaderboard should serialize to its save file
//NEEDS TESTING - Not sure if can serialize itself / load to itself -- Consider making a class to hold the list of scores and serialize/deserialize that instead
class Leaderboard {

public:
	//Singleton
	static Leaderboard* GetInstance();
	std::vector<Score> GetTopScores(int num = 10); //Get as many top scores as needed, grabs all from folder, decrypts and orders
	void SaveScore(); //Encrypts score and saves in folder
	void OrderScores();
	Score* GetCurrentScore() { return &currentScore; }


	friend class cereal::access;
	template<class Archive>
	void serialize(Archive &archive) {
		archive(scores);
	}
private:
	//Singleton
	Leaderboard();
	Leaderboard* instance;
	Score currentScore;

	std::vector<Score> scores;

	std::vector<Score> LoadScores(); //Get each score in file, Unserialize it, then insertion sort them

};
#endif
