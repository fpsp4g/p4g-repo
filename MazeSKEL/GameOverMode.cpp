#include "GameOverMode.h"
#include "GameModeManager.h"
using namespace DirectX;
using namespace DirectX::SimpleMath;


void GameOverMode::Update(float dTime)
{
	gmOvrMenu.Update(dTime);
	if (gmOvrMenu.isButtonPressed(0))
	{
		GameModeManager::GetInstance()->SwapMode(0);
	}

	GetGamepad()->Update();
}

void GameOverMode::Render(float dTime)
{
	BeginRender(Colours::Black);

	FX::MyFX& fx = *FX::GetMyFX();

	gmOvrMenu.Render(dTime);

	fx.mpSpriteB->Begin(SpriteSortMode_Deferred);

	//background
	int w, h;
	GetClientExtents(w, h);
	std::string text = "You have died!";
	//wchar_t* displayText = new wchar_t[text.size()/2];
	//std::copy(text.begin(), text.end(), displayText);
	std::wstring displayText(text.begin(), text.end());

	Vector2 origin = fx.mpFont->MeasureString(displayText.c_str()) / 2.f;
	fx.mpFont->DrawString(fx.mpSpriteB, displayText.c_str(), Vector2(w / 2, h / 2), Colors::Red, 0.f, origin);
	fx.mpSpriteB->End();

	EndRender();
}
void GameOverMode::KeyPressEvent()
{

}
