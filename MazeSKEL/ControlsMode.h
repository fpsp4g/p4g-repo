#ifndef ControlsModeH
#define ControlsModeH
#include "GameMode.h"
#include "CommonStates.h"
#include <algorithm>
#include <vector>
#include "Mesh.h"
#include "Model.h"
#include "FPSCamera.h"
#include "WindowUtils.h"
#include "D3D.h"
#include "GeometryBuilder.h"
#include "FX.h"
#include "Input.h"
#include "Camera.h"
#include "ControlsMenu.h"
class ControlsMode :
	public GameMode
{
public:
	void Update(float dTime);
	void Render(float dTime);
	void Initialise() { GetGamepad()->Update(); };
	void Release() {};
	void KeyPressEvent();

private:
	ControlsMenu menu;

};

#endif