#ifndef DoorBLOCKH
#define DoorBLOCKH

#include "Block.h"

class TriggerDoorBlock : public Block {

public:
	virtual void Trigger(bool pressed);
};

#endif