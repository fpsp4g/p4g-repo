/*******************************************************
* Copyright (C) 2018 Thomas Dunn tom1998dunn@hotmail.com
*
* This file is part of P4G Assignment.
*
* This code can not be copied and/or distributed without the express
* permission of Thomas Dunn
*******************************************************/

#ifndef LEVELMANAGERH
#define LEVELMANAGERH

#include <vector>
#include <string>
#include "FX.h"
#include "D3D.h"
#include "TextureRegionHandler.h"
#include <map>

#include "Player.h"

#include "Level.h"
#include "TestLevel.h"
#include "Level1.h"
#include "Level2.h"


class LevelManager {

public:
	LevelManager();
	~LevelManager();
	void Init(ID3D11ShaderResourceView* spriteSheetTexture);
	void Render(float dTime);
	void LoadLevel(const std::string& level);
	void SwitchLevel(const std::string& level, int id);			// Level + door id
	void ClearCache();
	ID3D11ShaderResourceView* GetSpriteSheetTexture();
	TextureRegionHandler* GetTextureHandler();
	static LevelManager* GetMyLevelManager();
	ID3D11VertexShader* vertexShader;
	ID3D11PixelShader* pixelShader;

	//ADDED BY CALLUM
	Level* GetCurrentLevel() { return currentLevel; }
	Player* GetPlayer() { return player; };
private:
	std::map<std::string, Level*> loadedLevels;

	ID3D11ShaderResourceView* spriteSheetTexture;

	Player* player;
	TextureRegionHandler* textureRegionHandler;
	Level* currentLevel;

};

#endif