#ifndef EnemyStateH
#define EnemyStateH

enum EnemyState
{
	IDLE, PATROL, CHASE, SEARCH, ATTACK
};


#endif