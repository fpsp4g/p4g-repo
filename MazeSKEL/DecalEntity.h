#ifndef DECALENTITYH
#define DECALENTITYH

#include "Entity.h"

class DecalEntity : public Entity {

public:
	DecalEntity(float x, float z): textureIndex(0), tint(1,1,1), scale(1,1,1){
		position = DirectX::SimpleMath::Vector3(x, 0, z);
	}

	DecalEntity() : textureIndex(0), tint(1, 1, 1), scale(1, 1, 1) {}

	virtual void Render(float dTime);
	virtual void Update(float dTime);

	const DirectX::SimpleMath::Vector3& GetTint() const { return tint; }
	const DirectX::SimpleMath::Vector3& GetRotation() const { return rotation; }
	const DirectX::SimpleMath::Vector3& GetScale() const { return scale; }
	void SetTint(const DirectX::SimpleMath::Vector3& tint);
	int GetTextureIndex() const { return textureIndex; }

protected:
	int textureIndex;							// The current texture we are using
	DirectX::SimpleMath::Vector3 tint;			// The tint of this decal
	DirectX::SimpleMath::Vector3 rotation;
	DirectX::SimpleMath::Vector3 scale;

};

#endif
