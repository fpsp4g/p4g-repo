/*******************************************************
* Copyright (C) 2018 Thomas Dunn tom1998dunn@hotmail.com
*
* This file is part of P4G Assignment.
*
* This code can not be copied and/or distributed without the express
* permission of Thomas Dunn
*******************************************************/

#ifndef LEVELH
#define LEVELH

#include "lodepng.h"
#include <string>
#include <vector>
#include "FX.h"
#include "Model.h"
#include "ShaderTypes.h"
#include "SimpleMath.h"
#include "Queue.h"
#include "LevelPointLightManager.h"

#include "Block.h"
#include "SoildBlock.h"
#include "GrassBlock.h"
#include "SpecialTileBlock.h"
#include "WaterBlock.h"
#include "LavaBlock.h"
#include "TriggerDoorBlock.h"

#include "Player.h"
#include "Door.h"
#include "DecalRenderer.h"
#include "EntityLight.h"
#include "ParticleEmitter.h"
#include "TorchEntity.h"
#include "ExplodeEmitter.h"
#include "DynamicDoor.h"
#include "TriggerDoor.h"
#include "GameWonDoor.h"

class Level {

public:
	Level() : player(nullptr),updateMesh(false), hasCreated(false), ceilingTexture(0), floorTexture(0),wallTexture(0), ceilingColour(0xffffff), wallColour(0xffffff), floorColour(0xffffff){}
	virtual ~Level();
	virtual void Init() = 0;							// Default settings for this level goes in this function
	virtual void SwitchLevel(int id) = 0;
	void CreateMap(const std::string& pngLocation);		// Creates the map using a method i made - Tom
	void Render(float dTime);							// Renders and updates the whole map including all entities
	Block* GetBlockAt(int x, int z);					// Gives access to a certain block using (X,Y)
	Node GetPlayerNode();
	void SetPlayer(Player* player) { this->player = player; }
	Player* GetPlayer() const { return player; }
	bool PlayerExists() const { return player != nullptr; } //Used to check if player exists when enemies want to rotate towards it
	void HighlightPath(const Queue<Node>* path);
	DecalRenderer* GetDecalRenderer() { return &decalRenderer; }		// This is my custom renderer
	void OnEnable();									// When level is activated
	void OnDisable();									// When level is deactivated
	void FindSpawn(int id);								// Finds player spawn
	void Trigger(int id, bool pressed);
	const DirectX::SimpleMath::Vector2& GetSpawnPosition() { return spawnPosition; }
protected:
	// Used to store the map data (Of the png)
	struct Image
	{
		std::vector<unsigned char> data;	// All pixel data from the image
		std::vector<long> hexdata;			// All pixel data into hex format
		std::vector<int> alphacodedata;		// All pixels 255 - alpha value 
		unsigned width;						// Image width
		unsigned height;					// Image height

	public: 
		// Gets a pixel at a certain point (Safe)
		long GetPixelAt(int x, int y) const {
			if (x < 0 || y < 0 || x >= width || y >= height) return 0x000000;
			return hexdata[y * width + x];
		}
	};

	long ceilingColour;
	long wallColour;
	long floorColour;

	int ceilingTexture;
	int floorTexture;
	int wallTexture;

	DirectX::SimpleMath::Vector2 spawnPosition;

private:
	// The map width and length
	unsigned mapWidth, mapLength;		//in the version I was given, these were unused. I needed them, so they have been set while setting up the level - Callum
	// All the blocks in order in the map
	std::vector<Block*> map;
	std::vector<SpecialTileBlock*> specialBlocks;  // This is only so we don't loop over all blocks just for find special blocks every time we update the mesh

	void Generate(const Image& image);
	unsigned long CreateRGB(int r, int g, int b) const;
	DirectX::SimpleMath::Vector3 HexToRGB(long hexCode) const;		// Uses the hex colour code provided in the blocks to turn into rgb values used for shader
	void GetPixelData(Image& image);
	void RenderLevel();									// Renders the actual level
	void RenderEntities(float dTime);					// Renders and updates entities
	void RenderBlock(Block* block, float dTime);		// Renders and updates a certain block entities
	void UpdateMesh();									// Update the mesh

	//CALLUM - Added x and y to this, because I need to pass that info to nodes / 
	//(Tom) as mentioned before this is not designed for position, you'll have to get the corriponding block using the map grid map[y * width + x]
	Block* CreateBlock(long hexCode);
	void CreateEntity(long hexCode, int x, int z, Block* currentBlock, Block* leftBlock, Block* rightBlock, Block* backBlock, Block* frontBlock);
	float GetYRotationFreeZone(Block* leftBlock, Block* rightBlock, Block* backBlock, Block* frontBlock) const; // This goes through each block around the entity to see which block is free so we know what direction to rotate it


	// Builds the walls of the block
	void BuildFloor(const Block& block,std::vector<VertexPosNormTexColor>*, std::vector<unsigned int>*,int x, int y);
	void BuildCeil(const Block& block, std::vector<VertexPosNormTexColor>*, std::vector<unsigned int>*, int x, int y);
	void BuildLeftWall(const Block& block, const Block& leftBlock, std::vector<VertexPosNormTexColor>*, std::vector<unsigned int>*, int x, int y);
	void BuildRightWall(const Block& block, const Block& rightBlock, std::vector<VertexPosNormTexColor>*, std::vector<unsigned int>*, int x, int y);
	void BuildFrontWall(const Block& block, const Block& frontBlock, std::vector<VertexPosNormTexColor>*, std::vector<unsigned int>*, int x, int y);
	void BuildBackWall(const Block& block, const Block& backBlock, std::vector<VertexPosNormTexColor>*, std::vector<unsigned int>*, int x, int y);
	//------------------


	ID3D11ShaderResourceView* spriteSheetTexture;				// The sprite sheet texture

	SoildBlock soildBlock;						// Its just a wall, not need to create multiple of these

	Player* player;								/* The current player (Which is in this level) - 
												Note: The player is only an instance for this level, 
												if you're moving to different levels and want 
												to keep data about player then please include 
												it outside of the level and use a singleton to access it.
												Otherwise it will not save across levels - Tom*/

	std::vector<VertexPosNormTexColor> posTexNormsColors;		// Holds data about how the map is contructed
	std::vector<unsigned int> indices;							// Holds data about how to render the map

	MaterialExt material;						// Material used for rendering
	ID3D11Buffer* vertexBuffer = nullptr;		// Vertex buffer used for rendering
	ID3D11Buffer* indexBuffer = nullptr;		// Index buffer	used for rendering
	int numIndices;								// Number of indices in the current mesh
	bool updateMesh;							// used for when we want to update the mesh for some reason (This will only be true when SpecialBlock attrib has changed)
	bool hasCreated;

	DecalRenderer decalRenderer;
};

#endif