#ifndef TUNNELLERDUST_H
#define TUNNELLERDUST_H

#include "ParticleEmitter.h"
#include <vector>
#include <random>

class TunnellerDust : public ParticleEmitter {

public:
	TunnellerDust(float x, float z);
	~TunnellerDust();
	virtual void Update(float dTime);		// This will be a custom method for each particle system to handle how a particle should spawn
protected:
	float time{ 0 };
private:

};

#endif
