#ifndef WATERBLOCKH
#define WATERBLOCKH

#include "LiquidBlock.h"

class WaterBlock : public LiquidBlock {

public:
	WaterBlock();

	virtual void Update(float dTime);

private:
	int textures[3] = { 16,17,18 };

	float time;
	int arrayIndex;
	int arraySize;
	const float ANIMATE_TIME;
};

#endif