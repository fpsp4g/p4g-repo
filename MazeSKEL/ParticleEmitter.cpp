#include "ParticleEmitter.h"
#include "Level.h"

ParticleEmitter::ParticleEmitter(float x, float z)
{
	position = DirectX::SimpleMath::Vector3(x, 0, z);
	isTriggerCollision = true;
}

ParticleEmitter::~ParticleEmitter()
{
	//auto it = particles.begin();
	//while (it != particles.end())
	//{
	//	(*it)->UnlinkEntityFromMap();
	//	delete (*it);
	//	it++;
	//}
}

void ParticleEmitter::Render(float dTime)
{
	//auto it = particles.begin();
	//while (it != particles.end())
	//{
	//	(*it)->HardUpdate(dTime);
	//	(*it)->Render(dTime);

	//	// When particle has been destoryed
	//	if ((*it)->HasRequestedDelete()) {
	//		Entity* temp = (*it);
	//		temp->UnlinkEntityFromMap();
	//		it = particles.erase(it);
	//		delete temp;
	//	}
	//	else
	//		it++;
	//}
}

void ParticleEmitter::Update(float dTime)
{
	if (level != nullptr) {
		time += dTime;
		if (time >= 0.1f) {
			time = 0;

			// Test
			float randomX = (((double)rand() / (RAND_MAX)) * 2) - 1;
			float randomZ = (((double)rand() / (RAND_MAX)) * 2) - 1;
			Particle* p = new Particle(position, DirectX::SimpleMath::Vector3(randomX, 1.0f, randomZ), 0.5f);
			p->SetTint(DirectX::SimpleMath::Vector3(((double)rand() / (RAND_MAX)) + 1, ((double)rand() / (RAND_MAX)) + 1, ((double)rand() / (RAND_MAX)) + 1));
			AddParticle(p);
		}
	}
}

void ParticleEmitter::AddParticle(Particle * particle)
{
	level->GetBlockAt(-1, -1)->sprites.push_back(particle);
	particle->LinkEntityToMap(level);
}
