#include "BillboardEntity.h"
#include "Level.h"

void BillboardEntity::HardUpdate(float dTime)
{
	DecalEntity::HardUpdate(dTime);
	FacePlayer();
}

void BillboardEntity::FacePlayer()
{
	using namespace DirectX::SimpleMath;

	// Rotates billboard to face player
	Vector3 playerPos = level->GetPlayer()->GetPosition();
	float angle = atan2(position.x - playerPos.x, position.z - playerPos.z) * (180.0 / PI);
	rotation.y = -angle;
	//-------------------------------
}
