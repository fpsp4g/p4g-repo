#ifndef CharacterH
#define CharacterH

#include "Entity.h"

class Character : public Entity
{
public:
	Character(int);

	virtual void Update(float dTime) = 0;
	virtual void Render(float dTime) = 0;

	void TakeDamage(int damage);
	void Heal(int amount);

	int GetCurrentHealth()	{ return hp; }
	int GetMaxHealth()		{ return maxHp; }

private:
	//need some stats now
	int hp, maxHp;

protected:
	bool visible = true;	//Characters may not always be visible (i.e. tunnellers are invisible while underground)
	virtual void Die() = 0; // Left Abstract since characters should never be created
};
#endif