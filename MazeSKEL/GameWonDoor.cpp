#include "GameWonDoor.h"
#include "GameModeManager.h"

GameWonDoor::GameWonDoor(float x, float z, float yRotation) : Door(x, z, yRotation)
{
	tint = Vector3(0.357f, 0.09f, 0.071f);
}

void GameWonDoor::Collision(Entity * entityCollidedWith, bool inXAxis, bool inZAxis)
{
	if (dynamic_cast<Player*>(entityCollidedWith)) 
	{
		if (!GameModeManager::GetInstance()->AwaitingSwap())
			GameModeManager::GetInstance()->SwapMode(4);
	}
}
