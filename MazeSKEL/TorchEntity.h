#ifndef TORCHENTITYH
#define TORCHENTITYH

#include "BillboardEntity.h"

class TorchEntity : public BillboardEntity {
	
public:
	TorchEntity(float x,float z);
	virtual void Update(float dTime);

private:
	float animationTime{ 0 };

};

#endif