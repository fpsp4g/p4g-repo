#ifndef LoadingGameModeH
#define LoadingGameModeH

#include "GameMode.h"
#include "CommonStates.h"
#include <algorithm>
#include <vector>
#include "Mesh.h"
#include "Model.h"
#include "FPSCamera.h"
#include "WindowUtils.h"
#include "D3D.h"
#include "GeometryBuilder.h"
#include "FX.h"
#include "Input.h"
#include "LevelManager.h"
#include "Camera.h"
#include "MainMenu.h"
#include <thread>
#include "WindowUtils.h"
#include <future>


class LoadingGameMode :
	public GameMode
{
public:
	LoadingGameMode();
	~LoadingGameMode();

	void Update(float dTime);
	void Render(float dTime);
	void Initialise();
	void Release();
	void KeyPressEvent();
	void LoadResources();

private:
	string loadingString;
	std::future<void> mLoadThread;
	float updateTime, currentTime;
	bool isDone = false;
};

#endif