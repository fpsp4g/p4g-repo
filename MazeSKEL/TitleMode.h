#ifndef TitleModeH
#define TitleModeH

#include "GameMode.h"
#include "CommonStates.h"
#include <algorithm>
#include <vector>
#include "Mesh.h"
#include "Model.h"
#include "FPSCamera.h"
#include "WindowUtils.h"
#include "D3D.h"
#include "GeometryBuilder.h"
#include "FX.h"
#include "Input.h"
#include "LevelManager.h"
#include "Camera.h"

#include "MainMenu.h"

class TitleMode :
	public GameMode
{
public:
	TitleMode();
	~TitleMode();

	void Update(float dTime);
	void Render(float dTime);
	void Initialise();
	void Release();
	void KeyPressEvent();

private:
	ID3D11ShaderResourceView * mpTitleTex = nullptr;
	DirectX::SimpleMath::Vector2 mTitleDim, mLoadingDim;
	ID3D11ShaderResourceView *mpLoadingTex = nullptr;
	static const float LOAD_DELAY_SECS;
	Camera mCamera;

	MainMenu mainMenu;

};
#endif