#include "GameWonMode.h"
#include "GameModeManager.h"
using namespace DirectX;
using namespace DirectX::SimpleMath;


void GameWonMode::Update(float dTime)
{
	gmWonMenu.Update(dTime);
	if (gmWonMenu.isButtonPressed(0))
	{
		GameModeManager::GetInstance()->SwapMode(0);
	}

	GetGamepad()->Update();
}

void GameWonMode::Render(float dTime)
{
	BeginRender(Colours::Black);

	FX::MyFX& fx = *FX::GetMyFX();

	gmWonMenu.Render(dTime);

	fx.mpSpriteB->Begin(SpriteSortMode_Deferred);

	//background
	int w, h;
	GetClientExtents(w, h);
	std::string text = "You win!";
	//wchar_t* displayText = new wchar_t[text.size()/2];
	//std::copy(text.begin(), text.end(), displayText);
	std::wstring displayText(text.begin(), text.end());

	Vector2 origin = fx.mpFont->MeasureString(displayText.c_str()) / 2.f;
	fx.mpFont->DrawString(fx.mpSpriteB, displayText.c_str(), Vector2(w / 2, h / 2), Colors::Red, 0.f, origin);
	fx.mpSpriteB->End();

	EndRender();
}
void GameWonMode::KeyPressEvent()
{

}
