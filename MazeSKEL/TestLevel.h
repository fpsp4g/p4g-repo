#ifndef TESTLEVELH
#define TESTLEVELH

#include "Level.h"

class TestLevel : public Level {

public:
	void Init();
	virtual void SwitchLevel(int id);

};

#endif TESTLEVELH