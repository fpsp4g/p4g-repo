#ifndef GameModeManagerH
#define GameModeManagerH
#include "GameMode.h"
#include "PlayingMode.h"
#include "TitleMode.h"
#include "LeaderboardMode.h"
#include "GameOverMode.h"
#include "GameWonMode.h"
#include "Game.h"
#include "LoadingGameMode.h"
#include "LevelManager.h"
#include "ControlsMode.h"

class GameModeManager
{
public:
	~GameModeManager();
	static GameModeManager* GetInstance();
	void SwapMode(int mode = 0);
	void SetGame(Game* game);
	bool AwaitingSwap();
	LevelManager* p_levelManager;

private:
	//Singleton
	GameModeManager();
	GameModeManager* instance;
	GameMode* temp;
	Game* game;
};







#endif

