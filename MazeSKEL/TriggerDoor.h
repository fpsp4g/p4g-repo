#ifndef TRIGGERDOORH
#define TRIGGERDOORH

#include "DynamicDoor.h"

class TriggerDoor : public DynamicDoor {
public:
	TriggerDoor(float x, float z, float yRotation);
	virtual void Trigger(bool pressed);
	virtual void Update(float dTime);

};

#endif