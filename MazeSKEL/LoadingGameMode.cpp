#include "LoadingGameMode.h"
#include "GameModeManager.h"


LoadingGameMode::LoadingGameMode()
{
	loadingString = "Loading";
	updateTime = 0.2f;
	currentTime = 0;
}


LoadingGameMode::~LoadingGameMode()
{
}

void LoadingGameMode::Update(float dTime)
{
	currentTime += dTime;
	if (currentTime > updateTime) {
		currentTime = 0;

		if (loadingString == "Loading") {
			loadingString = "Loading.";
		}
		else if (loadingString == "Loading.") {
			loadingString = "Loading..";
		}
		else if (loadingString == "Loading..") {
			loadingString = "Loading...";
		}
		else if (loadingString == "Loading...") {
			loadingString = "Loading";
		}
	}

	if (isDone) {
		GameModeManager::GetInstance()->SwapMode(1);
	}
}

void LoadingGameMode::Render(float dTime)
{
	BeginRender(Colours::Black);
	int w, h;
	GetClientExtents(w, h);
	//FX::GetMyFX()->mpFont->DrawString(FX::GetMyFX()->mpSpriteB, loadingString.c_str(), Vector2(500,200), Colors::White, 0, Vector2(0, 0),Vector2(1,1))
	std::wstring displayText(loadingString.begin(), loadingString.end());;
	Vector2 origin = FX::GetMyFX()->mpFont->MeasureString(displayText.c_str()) / 2.f;
	FX::GetMyFX()->mpSpriteB->Begin(SpriteSortMode_Deferred);
	FX::GetMyFX()->mpFont->DrawString(FX::GetMyFX()->mpSpriteB, displayText.c_str(), Vector2(w/2, h/2), Colors::Red, 0.f, origin);
	FX::GetMyFX()->mpSpriteB->End();
	EndRender();
}

void LoadingGameMode::Initialise()
{
	mLoadThread = std::async(launch::async, &LoadingGameMode::LoadResources, this);
}

void LoadingGameMode::Release()
{

}

void LoadingGameMode::KeyPressEvent()
{
}

void LoadingGameMode::LoadResources()
{
	new MeshManager;
	ID3D11ShaderResourceView* leveltex = FX::GetMyFX()->mCache.LoadTexture("data/spritesheet.dds", false, gd3dDevice);
	LevelManager* tempLevel = new LevelManager();
	tempLevel->Init(leveltex);
	//new EnemyManager;
	//EnemyManager::GetInstance()->SetLevel(levelManager.GetCurrentLevel());

	//music
	Mesh& sb = MeshManager::Get()->CreateMesh("skybox");			// <-- This will most likely crash when loaded again beware! - Tom
	sb.CreateFrom("data/skybox.fbx", gd3dDevice, FX::GetMyFX()->mCache);

	Mesh& m1 = MeshManager::Get()->CreateMesh("PABHC_Payload");
	m1.CreateFrom("data/projectiles/PABHC_Payload.fbx", gd3dDevice, FX::GetMyFX()->mCache);

	Mesh& m2 = MeshManager::Get()->CreateMesh("Enemy_Projectile");
	m2.CreateFrom("data/projectiles/Enemy_Projectile.fbx", gd3dDevice, FX::GetMyFX()->mCache);
	GameModeManager::GetInstance()->p_levelManager = tempLevel;
	isDone = true;
}
