#include "TorchEntity.h"

TorchEntity::TorchEntity(float x,float z)
{
	position = DirectX::SimpleMath::Vector3(x,0.0f, z);

	isTriggerCollision = true;
	textureIndex = 96;
	tint = DirectX::SimpleMath::Vector3(1.000, 0.549, 0.000);
}

void TorchEntity::Update(float dTime)
{
	LevelPointLightManager::GetInstance()->Activate(GetDistanceFromPlayer(), DirectX::SimpleMath::Vector3((int)position.x + .5f,position.y + .5f,(int)position.z + .5f), tint,1.0f,1.0f);

	animationTime += dTime;

	if (animationTime >= 0.2f) {
		animationTime = 0;

		textureIndex++;

		if (textureIndex > 97)
			textureIndex = 96;
	}
}
