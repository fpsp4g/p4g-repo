#include "TitleMode.h"
#include "GameModeManager.h"
using namespace DirectX;
using namespace DirectX::SimpleMath;
TitleMode::TitleMode()
{
}


TitleMode::~TitleMode()
{
}

void TitleMode::Update(float dTime)
{
	mainMenu.Update(dTime);
	if (mainMenu.isButtonPressed(0))
	{
		GameModeManager::GetInstance()->SwapMode(9);
	}
	else if (mainMenu.isButtonPressed(1))
	{
		GameModeManager::GetInstance()->SwapMode(2);
	}
	else if (mainMenu.isButtonPressed(3))
	{
		PostQuitMessage(0);
	}
	else if (mainMenu.isButtonPressed(2))
	{
		GameModeManager::GetInstance()->SwapMode(8);
	}

	GetGamepad()->Update();
}

void TitleMode::Render(float dTime)
{
	BeginRender(Colours::Black);

	FX::MyFX& fx = *FX::GetMyFX();

	mainMenu.Render(dTime);

	EndRender();
}

void TitleMode::Initialise()
{
	GetIAudioMgr()->GetSongMgr()->Play("credits", true, false, &musicHdl, 0.05);
	//Load all title textures we need
	FX::MyFX& fx = *FX::GetMyFX();

	GetGamepad()->SetDeadZone(0.25, 0.25);
	GetGamepad()->Update();
}

void TitleMode::Release()
{
	//Delete any heap data
	GetIAudioMgr()->GetSongMgr()->Stop();
}

void TitleMode::KeyPressEvent()
{

}
