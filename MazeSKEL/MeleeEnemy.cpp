#include "MeleeEnemy.h"

MeleeEnemy::MeleeEnemy(Vector3 pos) : Enemy("MeleeDrone", 100, Vector3(pos.x, 0.5, pos.z), Vector3(0.005f, 0.005f, 0.005f), Vector3(0, 0, 0), 2)
{
}

MeleeEnemy::~MeleeEnemy()
{
}

void MeleeEnemy::SearchBehaviour(float dTime)
{
	if (CanSeeTarget(level->GetPlayerNode())) //if you can see the player...
	{
		state = EnemyState::CHASE;
	}
	else
	{
		if (!pathRequested) //if the enemy hasn't asked for a path yet, do it
		{
			PathTo(level->GetPlayerNode());
			pathRequested = true;
		}
		else if (pathFound)
		{
			//move towards the next node
			Vector3 nodePos(currentPath.first().getX() + 0.5, GetPosition().y, currentPath.first().getY() + 0.5);
			MoveTowards(nodePos, dTime);

			if (Vector3::Distance(nodePos, GetPosition()) < 0.1)
			{
				currentPath.removeFirst();
				if (currentPath.length() == 0) //if the enemy has finished searching the path, give up looking
				{
					pathRequested = false;
					pathFound = false;
					state = EnemyState::PATROL;
				}
			}
		}
	}
}
void MeleeEnemy::PatrolBehaviour(float dTime) //return to spawn position
{
	if (!CanSeeTarget(level->GetPlayerNode())) //if you can't see the player...
	{
		MoveTowards(Vector3(spawnPos.x, GetPosition().y, spawnPos.z), dTime);

		if (Vector3::Distance(spawnPos, GetPosition()) < 0.1)
		{
			state = EnemyState::IDLE;
		}
	}
	else
	{
		state = EnemyState::CHASE;
	}
}
void MeleeEnemy::IdleBehaviour(float dTime)
{
	if (CanSeeTarget(level->GetPlayerNode())) //if you can see the player...
	{
		state = EnemyState::CHASE;	//give chase
	}
}
void MeleeEnemy::AttackBehaviour(float dTime)
{
	
}
void MeleeEnemy::ChaseBahaviour(float dTime)
{
	if (CanSeeTarget(level->GetPlayerNode())) //if you can see the player...
	{
		//move towards the player
		MoveTowards(Vector3(level->GetPlayerNode().getX() + 0.5, GetPosition().y, level->GetPlayerNode().getY() + 0.5), dTime);

		/*if (Vector3::Distance(level->GetPlayer()->GetPosition(), GetPosition()) < 0.75)
		{
			state = EnemyState::ATTACK;
		}*/

	}
	else
	{
		state = EnemyState::SEARCH;
	}
}

void MeleeEnemy::Collision(Entity* entityCollidedWith, bool inXAxis, bool inZAxis)
{
	if (dynamic_cast<Player*>(entityCollidedWith))
	{
		level->GetPlayer()->TakeDamage(100);//for the time being, the player will be one shot, because that is easier to test
		state = EnemyState::CHASE;
	}
}

void MeleeEnemy::Die()
{
	Enemy::Die();

	// Adds exploding particles
	ExplodeEmitter* pSystem = new ExplodeEmitter(position.x, position.z);
	level->GetBlockAt(-1,-1)->sprites.push_back(pSystem);
	pSystem->LinkEntityToMap(level);
}
