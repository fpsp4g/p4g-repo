#include "Level1.h"
#include "LevelManager.h"

void Level1::Init() {

	wallTexture = 1;
	wallColour = 0xA01C1C;
	floorColour = 0x7F3F3F;
	ceilingColour = 0x7F3F3F;
	CreateMap("data/levels/level2.png");
}

void Level1::SwitchLevel(int id)
{
	if (id == 1) LevelManager::GetMyLevelManager()->SwitchLevel("TestLevel",1);
}
