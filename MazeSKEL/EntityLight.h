#ifndef ENTITYLIGHTH
#define ENTITYLIGHTH

// This is only a entity for lights on blocks

#include "Entity.h"
#include "Level.h"

class EntityLight : public Entity {

public:
	EntityLight(const DirectX::SimpleMath::Vector3& position_,const DirectX::SimpleMath::Vector3& lightColour_, float lightRange_ = 10.0f, float lightAtten_ = 0.5f);
	virtual void Render(float dTime);
	virtual void Update(float dTime);

private:
	DirectX::SimpleMath::Vector3 lightColour;
	float lightRange,lightAtten;
};

#endif