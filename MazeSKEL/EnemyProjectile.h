#ifndef PROJECTILE_ENEMY_H
#define PROJECTILE_ENEMY_H

#include "Projectile.h"

class EnemyProjectile : public Projectile
{
public:
	EnemyProjectile(Vector3 pos, Vector3 rotation, Vector3 dir);
	~EnemyProjectile();
	virtual void Update(float dTime);
	virtual DirectX::BoundingBox GetBoundingBox();
	virtual void OnLevelCollision(bool inXAxis, bool inZAxis);
	virtual void Collision(Entity* entityCollidedWith, bool inXAxis, bool inZAxis);

private:
	unsigned int wallHitSound;
};

#endif