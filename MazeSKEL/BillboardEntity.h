#ifndef BILLBOARDENTITYH
#define BILLBOARDENTITYH

#include "DecalEntity.h"

class BillboardEntity : public DecalEntity {

public:
	virtual void HardUpdate(float dTime);
private:
	void FacePlayer();

};

#endif