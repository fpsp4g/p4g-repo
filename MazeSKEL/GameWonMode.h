#ifndef GameWonModeH
#define GameWonModeH

#include "GameMode.h"
#include "CommonStates.h"
#include <algorithm>
#include <vector>
#include "Mesh.h"
#include "Model.h"
#include "FPSCamera.h"
#include "WindowUtils.h"
#include "D3D.h"
#include "GeometryBuilder.h"
#include "FX.h"
#include "Input.h"
#include "LevelManager.h"
#include "Camera.h"

#include "GameEndMenu.h"

class GameWonMode :
	public GameMode
{
public:

	void Update(float dTime);
	void Render(float dTime);
	void Initialise() { GetGamepad()->Update(); };
	void Release() {};
	void KeyPressEvent();

private:
	GameEndMenu gmWonMenu;


};
#endif
