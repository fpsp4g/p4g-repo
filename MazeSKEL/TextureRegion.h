/*******************************************************
* Copyright (C) 2018 Thomas Dunn tom1998dunn@hotmail.com
*
* This file is part of P4G Assignment.
*
* This code can not be copied and/or distributed without the express
* permission of Thomas Dunn
*******************************************************/
#ifndef TEXTUREREGIONH
#define TEXTUREREGIONH

#include "FX.h"
#include "D3D.h"
#include "D3DUtil.h"

class TextureRegion {

public:
	TextureRegion(ID3D11ShaderResourceView* texture_,float x, float y,float width,float height);

	float GetMinU() const;
	float GetMinV() const;
	float GetMaxU() const;
	float GetMaxV() const;

private:
	float minU, minV, maxU, maxV;
	ID3D11ShaderResourceView* texture;

};

#endif
