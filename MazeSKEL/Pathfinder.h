#ifndef PathfinderH
#define PathfinderH

#include <vector>
#include "Node.h"
#include "Queue.h"
#include "Level.h"
class Pathfinder
{
public:
	Pathfinder()
	{
		_ASSERT(instance == nullptr);
		instance = this;
		
	}
	~Pathfinder()
	{
		instance = nullptr;
	}
	static Pathfinder* GetInstance()
	{
		_ASSERT(instance);
		return instance;
	}
	void FindPath(Node, Node, Queue<Node>&, bool*);

	void LinkToLevel(Level* level) { this->level = level; }
	void DrawStraightLine(Node, Node, bool*, bool*);
	Vector3 FindRandomWalkableTilePos(float dTime, Node node);
private:
	std::vector<Node> nodeMap;
	static Pathfinder* instance;
	Level* level = nullptr;
};
#endif