#ifndef EnemyManagerH
#define EnemyManagerH

#include <vector>
#include "Enemy.h"
#include "MeleeEnemy.h"
#include "RangedEnemy.h"
#include "TurretEnemy.h"
#include "Level.h"

class EnemyManager
{
public:
	EnemyManager() 
	{
		
	}
	~EnemyManager() 
	{
		
	}
	static EnemyManager* GetInstance()
	{
		static EnemyManager instance;
		return &instance;
	}
	void Release();
	void SetLevel(Level* newLevel) 
	{
		level = newLevel;
	}
	Enemy* SpawnEnemy(Vector3);
	void RenderEnemies(float dTime);
	void UpdateEnemies(float dTime);
private:
	//void CheckForDeadEnemies();	//used to avoid circular dependancies
	std::vector<Enemy*> Enemies;
	EnemyManager* instance;
	Level* level = nullptr;
};
#endif