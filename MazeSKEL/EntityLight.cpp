#include "EntityLight.h"

EntityLight::EntityLight(const DirectX::SimpleMath::Vector3& position_,const DirectX::SimpleMath::Vector3 & lightColour_,float lightRange_,float lightAtten_) : lightColour(lightColour_), lightRange(lightRange_),lightAtten(lightAtten_)
{
	isTriggerCollision = true;
	position = position_;
}

void EntityLight::Render(float dTime)
{
	LevelPointLightManager::GetInstance()->Activate(GetDistanceFromPlayer(), position, lightColour,lightRange,lightAtten);
}

void EntityLight::Update(float dTime)
{
}
