#ifndef SPECIALTILEBLOCK
#define SPECIALTILEBLOCK

#include "Block.h"

class SpecialTileBlock : public Block {
public:
	SpecialTileBlock() :Block(false), requiresUpdate(false) {
		hasCollision = false;
		floorTextureID = 0; 
	}

	void SetFloorTexture(int textureID);
	void SetFloorColor(long color);

	int vertexIndex;		// The vertex index at which this block starts at

	bool requiresUpdate;	// if this tile block needs updating

};

#endif