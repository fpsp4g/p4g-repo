#include "Cursor.h"

// Includes for UI
#include "WindowUtils.h"
#include "CommonStates.h"

Cursor::Cursor()
{
	FX::GetMyFX()->mCache.LoadTexture("ui/cursor.dds", true, gd3dDevice);
	cursorTexIdx = FX::GetMyFX()->mCache.GetIdx("ui/cursor.dds");

	cursorCenter = Vector2(FX::GetMyFX()->mCache.Get(cursorTexIdx).dim.x / 2, FX::GetMyFX()->mCache.Get(cursorTexIdx).dim.y / 2);

	int w, h;
	GetClientExtents(w, h);

	UpdatePosition(w / 2, h / 2);
}
Cursor::~Cursor()
{

}

void Cursor::Render(float dTime)
{
	FX::MyFX& fx = *FX::GetMyFX();
	CommonStates state(gd3dDevice);
	fx.mpSpriteB->Begin(SpriteSortMode_Deferred, state.NonPremultiplied());

	Vector2 scale(0.2);
	FX::GetMyFX()->mpSpriteB->Draw(FX::GetMyFX()->mCache.Get(cursorTexIdx).pTex, pos - (cursorCenter * scale), nullptr, Colours::White, 0, Vector2(0, 0), scale);

	fx.mpSpriteB->End();
}

void Cursor::UpdatePosition(int x, int y)
{
	// Window info
	int w, h;
	GetClientExtents(w, h);

	pos.x += x;
	if (pos.x  < 0)
		pos.x = 0;
	else if (pos.x  > w)
		pos.x = w;

	pos.y += y;
	if (pos.y < 0)
		pos.y = 0;
	else if (pos.y > h)
		pos.y = h;
}