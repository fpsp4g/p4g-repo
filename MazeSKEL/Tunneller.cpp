#include "Tunneller.h"
#include "Pathfinder.h"
#include <math.h>
Tunneller::Tunneller(Vector3 pos, float dTime, Level* level) : TurretEnemy(pos), dust(new TunnellerDust(pos.x, pos.z))
{
	isTriggerCollision = true;
	visible = false;
	level->GetBlockAt(-1,-1)->sprites.push_back(dust);
	dust->LinkEntityToMap(level);
	endLocation = Pathfinder::GetInstance()->FindRandomWalkableTilePos(dTime, level->GetBlockAt(pos.x, pos.z)->GetNode());

	if (GetIAudioMgr()->GetSfxMgr()->IsPlaying(digSound) == false)
		GetIAudioMgr()->GetSfxMgr()->Play("82722__prozaciswack__digging", false, false, &digSound, 0.5 / (level->GetPlayer()->GetPosition() - position).Length());
	else
		GetIAudioMgr()->GetSfxMgr()->Stop(digSound);
	
}
Tunneller::~Tunneller()
{

}
void Tunneller::IdleBehaviour(float dTime)
{
	if ((abs(GetPosition().x - endLocation.x) < 1) && (abs(GetPosition().z - endLocation.z) < 1))
	{
		if (dust != nullptr)
		{
			GetIAudioMgr()->GetSfxMgr()->Stop(digSound);
			isTriggerCollision = false;
			visible = true;
			dust->DestroySelf();
			dust = nullptr;
		}

		//tunnellers can always see the player
		if (attackTimer <= 0)
		{
			attackTimer = maxAttackTime;
			state = EnemyState::ATTACK;
		}
		else
		{
			attackTimer -= dTime;
		}
		
	}
	else	//tunnel
	{
		
		MoveTowards(endLocation, dTime);
		dust->SetPosition(GetPosition());
	}
}
void Tunneller::AttackBehaviour(float dTime)
{
	//ATTACK
	EnemyProjectile* bullet = new EnemyProjectile(Vector3(position.x, position.y + 0.6, position.z), Vector3(0, GetYRotation(), 0), forward);

	if (GetIAudioMgr()->GetSfxMgr()->IsPlaying(shotSound) == false)
		GetIAudioMgr()->GetSfxMgr()->Play("275151__bird-man__gun-shot", false, false, &shotSound, 0.5 / (level->GetPlayer()->GetPosition() - position).Length());
	else
		GetIAudioMgr()->GetSfxMgr()->Stop(shotSound);

	level->GetBlockAt(position.x, position.z)->sprites.push_back(bullet);	//stops the rendering iterator becoming invalid
	bullet->LinkEntityToMap(level);

	//bullet = new EnemyProjectile(Vector3(position.x, position.y + 0.5, position.z), Vector3(0, GetYRotation() + D2R(5), 0), XMVector2TransformNormal(forward, Matrix::CreateRotationY(D2R(5))));
	//level->GetBlockAt(position.x + 1, position.z)->sprites.push_back(bullet);
	//bullet->LinkEntityToMap(level);

	//bullet = new EnemyProjectile(Vector3(position.x, position.y + 0.5, position.z), Vector3(0, GetYRotation() - D2R(5), 0), XMVector2TransformNormal(forward, Matrix::CreateRotationY(D2R(-5))));
	//level->GetBlockAt(position.x + 1, position.z)->sprites.push_back(bullet);
	//bullet->LinkEntityToMap(level);

	//===================
	state = EnemyState::IDLE;
}

