#ifndef LEVELPOINTLIGHTMANAGERH
#define LEVELPOINTLIGHTMANAGERH

#include "d3d.h"
#include "FX.h"
#include "vector"
#include <algorithm>

class LevelPointLightManager {

public:
	LevelPointLightManager();
	static LevelPointLightManager* GetInstance() {
		static LevelPointLightManager lightManager;
		return &lightManager;
	}
	void Activate(float disToPlayer, DirectX::SimpleMath::Vector3 position, DirectX::SimpleMath::Vector3 color, float range = 10.0f, float atten = 0.5f);
	void Bind();

private:

	void BindLights();
	void SortLights();

	struct Data {
		float disToPlayer;
		DirectX::SimpleMath::Vector3 lightPosition;
		DirectX::SimpleMath::Vector3 lightColour;
		float range;
		float atten;

		bool operator<(const Data& data) {
			return this->disToPlayer < data.disToPlayer;
		}
	};

	std::vector<Data> managedData;

	const float MAX_DISTANCE = 100;			// The max distance away from the player which a light can be rendered
	const int MAX_LIGHTS_MANAGED = 256;		// The amount of lights which can be filtered
	const int MAX_LIGHTS = 15;				// Amount of lights which can be rendered (31 because light pos 1 is not free)
	int managedLights = 0;					// The current amount of lights which have been managed (This frame)

};

#endif