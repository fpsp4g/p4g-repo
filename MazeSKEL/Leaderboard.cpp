#include "Leaderboard.h"

void Leaderboard::OrderScores()
{

	int j, length;
	Score temp;
	length = scores.size();

	for (int i = 0; i < length; i++) {
		j = i;

		while (j > 0 && scores[j].GetScore() > scores[j - 1].GetScore()) {
			temp = scores[j];
			scores[j] = scores[j - 1];
			scores[j - 1] = temp;
			j--;
		}
	}
}



Leaderboard::Leaderboard()
{
	//Check if we have leaderboard file
	std::ifstream infile("data/scores/leaderboard.data");
	if (infile.good()) {
		////File exists so load it in
		//std::ifstream ss("data/scores/leaderboard.data");
		cereal::BinaryInputArchive iarchive(infile); // Create an input archive
		iarchive(*this); // Read the data from the archive
	}
	else {
		//File doesn't exist so just start with a blank leaderboard
	}
}



Leaderboard * Leaderboard::GetInstance()
{
	static Leaderboard instance;
	return &instance;
}

std::vector<Score> Leaderboard::GetTopScores(int num)
{
	int looper = num;
	std::vector<Score> topX;
	if (num > scores.size())
		looper = scores.size();

	for (int i = 0; i < looper; i++)
	{
		topX.push_back(scores[i]);
	}
	return topX;
}


void Leaderboard::SaveScore()
{
	scores.push_back(currentScore);
	OrderScores();

	std::ofstream ss("data/scores/leaderboard.data");
	{
		cereal::BinaryOutputArchive oarchive(ss);
		oarchive(*this);
	}
	currentScore = Score();

}

