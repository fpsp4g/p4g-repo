#ifndef ScoreH
#define ScoreH

#include <string>
#include <cereal\archives\binary.hpp>
#include <cereal/types/vector.hpp>
#include <fstream>

class Score {
public:
	Score(int, std::string);
	Score();
	int GetScore() const;
	std::string GetName() const;
	void ChangeScore(int i);

	friend class cereal::access;
	template<class Archive>
	void serialize(Archive &archive) {
		archive(score,name);
	}
private:
	int score;
	std::string name;
};

#endif