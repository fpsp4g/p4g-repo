#include "LeaderboardMode.h"
#include "GameModeManager.h"
using namespace DirectX;
using namespace DirectX::SimpleMath;


LeaderboardMode::LeaderboardMode()
{
}


LeaderboardMode::~LeaderboardMode()
{
}

void LeaderboardMode::Initialise()
{
	scoresToShow = Leaderboard::GetInstance()->GetTopScores(10);

	GetGamepad()->Update();
}

void LeaderboardMode::Update(float dTime)
{
	ldrbrdMenu.Update(dTime);
	if (ldrbrdMenu.isButtonPressed(0))
	{
		GameModeManager::GetInstance()->SwapMode(0);
	}

	GetGamepad()->Update();
}

void LeaderboardMode::Render(float dTime)
{
	BeginRender(Colours::Black);

	FX::MyFX& fx = *FX::GetMyFX();

	ldrbrdMenu.Render(dTime);

	fx.mpSpriteB->Begin(SpriteSortMode_Deferred);

	//background
	int w, h;
	GetClientExtents(w, h);
	std::string text = "TOP SCORES\n";
	for (int i = 0; i < scoresToShow.size(); i++)
	{
		text+=  scoresToShow[i].GetName();
		text += "  ";
		text += std::to_string(scoresToShow[i].GetScore()) + "\n";
	}
	text = text.c_str();
	//wchar_t* displayText = new wchar_t[text.size()/2];
	//std::copy(text.begin(), text.end(), displayText);
	std::wstring displayText(text.begin(), text.end());
	
	Vector2 origin = fx.mpFont->MeasureString(displayText.c_str()) / 2.f;
	fx.mpFont->DrawString(fx.mpSpriteB, displayText.c_str(), Vector2(w / 2, h/2),Colors::Red,0.f,origin);
	fx.mpSpriteB->End();

	EndRender();
}

void LeaderboardMode::Release()
{
}

void LeaderboardMode::KeyPressEvent()
{
//#define MAINMENU VK_0
//
//
//	bool menu = GetMouseAndKeys()->IsPressed(MAINMENU);
//
//	if (menu) {
//		GameModeManager::GetInstance()->SwapMode(0);
//	}
}
