#ifndef PlayingModeH
#define PlayingModeH
#include "GameMode.h"
#include "CommonStates.h"
#include <algorithm>
#include <vector>
#include "Mesh.h"
#include "Model.h"
#include "FPSCamera.h"
#include "WindowUtils.h"
#include "D3D.h"
#include "GeometryBuilder.h"
#include "FX.h"
#include "Input.h"
#include "LevelManager.h"
#include "EnemyManager.h"
#include "Camera.h"
#include "RequestHandler.h"

#include "PauseMenu.h"

class PlayingGameMode :
	public GameMode
{
public:
	PlayingGameMode();
	PlayingGameMode(LevelManager * levelManager);
	~PlayingGameMode();

	 void Update(float dTime);
	 void Render(float dTime);
	 void Initialise();
	 void Release();
	 void KeyPressEvent();

	 Model mSkybox, testM;

private:
	LevelManager* levelManager;
	ID3D11ShaderResourceView* spriteSheetTexture;

	PauseMenu pauseMenu;

};

#endif