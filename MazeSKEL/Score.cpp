#include "Score.h"

int Score::GetScore() const
{
	return score;
}

std::string Score::GetName() const
{
	return name;
}

void Score::ChangeScore(int i)
{
	score += i;
}

Score::Score(int score, std::string name) :score(score), name(name) {}

Score::Score() {
	name = "Player";
	score = 0;
}

