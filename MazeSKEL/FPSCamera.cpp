#include "FPSCamera.h"
#include "D3D.h"
#include "D3DUtil.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

void FPSCamera::Initialise(Vector3* pos, const DirectX::SimpleMath::Vector3& tgt, Matrix& viewSpaceTfm)
{
	pitch = 0;
	yaw = 0;
	roll = 0;
	mpViewSpaceTfm = &viewSpaceTfm;
	mCamPos = pos;
	CreateViewMatrix(*mpViewSpaceTfm, *pos, tgt, Vector3(0, 1, 0));
}

void FPSCamera::Rotate(float dTime, float _yaw, float _pitch, float _roll, bool controllerEnabled)
{
	float pitchBeforeUpdate = pitch;

	if(controllerEnabled)
		yaw += (_yaw * camRotateSpeed) * dTime;
	else
		yaw += (_yaw * camRotateSpeed) * 0.01f;

	if(controllerEnabled)
		pitch += (_pitch * camRotateSpeed) * dTime;
	else
		pitch += (_pitch * camRotateSpeed) * 0.01f;

	if(controllerEnabled)
		roll += (_roll * camRotateSpeed) * dTime;
	else
		roll += (_roll * camRotateSpeed) * 0.01f;

	// Clamps the camera (Stops head going backwards)
	Matrix ori = Matrix::CreateFromYawPitchRoll(yaw, pitch, roll);
	Vector3 dir(0, 0, 1);
	dir = Vector3::TransformNormal(dir, ori);
	float pitchLimit = 0.75f;
	if (!(dir.y <= pitchLimit && dir.y >= -pitchLimit))
		pitch = pitchBeforeUpdate;

	UpdateViewMatrix(dTime);
}

void FPSCamera::UpdateViewMatrix(float dTime)
{
	Matrix ori = Matrix::CreateFromYawPitchRoll(yaw, pitch, roll);
	Vector3 dir(0, 0, 1), up(0, 1, 0);
	dir = Vector3::TransformNormal(dir, ori);
	up = Vector3::TransformNormal(up, ori);

	CreateViewMatrix(*mpViewSpaceTfm, *mCamPos, (*mCamPos) + dir, up);
}