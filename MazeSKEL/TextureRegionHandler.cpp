/*******************************************************
* Copyright (C) 2018 Thomas Dunn tom1998dunn@hotmail.com
*
* This file is part of P4G Assignment.
*
* This code can not be copied and/or distributed without the express
* permission of Thomas Dunn
*******************************************************/

#include "TextureRegionHandler.h"

TextureRegionHandler::TextureRegionHandler(ID3D11ShaderResourceView* texture, int split)
{
	this->texture = texture;
	DirectX::SimpleMath::Vector2 textureDim = FX::GetMyFX()->mCache.GetDimensions(texture);

	int amountX = textureDim.x / split;
	int amountY = textureDim.x / split;

	// splits the texture into regions
	for (int y = 0; y < amountY; ++y) {
		{
			for (int x = 0; x < amountX; ++x)
				regions.push_back(TextureRegion(texture, x * split, y * split, split, split));
		}
	}
}

const TextureRegion& TextureRegionHandler::GetTexture(int index) const
{
	return regions[index];
}
