#include "Game.h"
#include "RequestHandler.h"
#include "GameModeManager.h"
using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;


void Game::OnResize(int screenWidth, int screenHeight)
{
	OnResize_Default(screenWidth, screenHeight);
}

void Game::Initialise()
{
}

void Game::Release()
{
	if (currentGameMode != nullptr) {
		currentGameMode->Release();
		delete currentGameMode;
		currentGameMode = nullptr;
	}
}

void Game::Update(float dTime)
{
	//We only swap game modes at the start of an update loop so check if we need to do that
	if (GameModeManager::GetInstance()->AwaitingSwap()) {
		GameModeManager::GetInstance()->SwapMode();

	}

	//Check if our mode needs to be swapped for this round
	if(currentGameMode != nullptr) // Pleasse ensure this is not null - Tom
	currentGameMode->Update(dTime);

}

void Game::Render(float dTime)
{
	if (currentGameMode != nullptr) // Pleasse ensure this is not null - Tom
	currentGameMode->Render(dTime);
}

LRESULT Game::WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	const float camInc = 200.f * GetElapsedSec();

	switch (msg)
	{
	case WM_INPUT:
		GetMouseAndKeys()->MessageEvent((HRAWINPUT)lParam);
		break;
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
			if(!dynamic_cast<PlayingGameMode*>(currentGameMode))
			{
				PostQuitMessage(0);
				return 0;
			}
		default:
			currentGameMode->KeyPressEvent(); // If we aren't clicking escape then check if our game mode can support the input
		}
	}

	//default message handling (resize window, full screen, etc)
	return DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

