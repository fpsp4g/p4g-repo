/*******************************************************
* Copyright (C) 2018 Thomas Dunn tom1998dunn@hotmail.com
*
* This file is part of P4G Assignment.
*
* This code can not be copied and/or distributed without the express
* permission of Thomas Dunn
*******************************************************/

#include "Level.h"

#include "LevelManager.h"
#include "MeleeEnemy.h"
#include "RangedEnemy.h"
#include "TurretEnemy.h"
#include "Boss.h"

#include "BouncerPickup.h"


using namespace DirectX::SimpleMath;

Level::~Level() {

	// Delete map
	for (Block* b : map) {
		if (b != &soildBlock) {
			delete b;	// As emptyBlock isn't on the heap
		}
	}

	if (vertexBuffer)
		vertexBuffer->Release();

	if (indexBuffer)
		indexBuffer->Release();

	decalRenderer.Release();

	posTexNormsColors.clear();
	indices.clear();
}

Block* Level::CreateBlock(long hexCode) {

	switch (hexCode) {
	case 0xffffff:
		return new Block(false); // <-- This should be default to false. But crashes.....
	case 0x7F006E:
		return new SpecialTileBlock();
	case 0x808080:
		return &soildBlock;
	case 0x00137F:
		return new WaterBlock();
	case 0xFF6A00:
		return new LavaBlock();
	case 0xFF4FCA:
		return new TriggerDoorBlock();
	}

	return new Block(false);
}

void Level::CreateEntity(long hexCode, int x, int z, Block* currentBlock, Block* leftBlock, Block* rightBlock, Block* backBlock, Block* frontBlock) {	// Please do not make this a vector (x,z) - Tom
	Entity* temp(nullptr);
	switch (hexCode) {
	case 0xff6699:
		//temp = EnemyManager::GetInstance()->SpawnEnemy(Vector3(x + 0.5, 0, z + 0.5)); //Test of spawning enemy LIAM
		temp = new MeleeEnemy(Vector3(x + 0.5, 0, z + 0.5));
		break;
	case 0xff00ff:
		temp = new RangedEnemy(Vector3(x + 0.5, 0, z + 0.5));
		break;
	case 0x00ff00:
		temp = new TurretEnemy(Vector3(x + 0.5, 0, z + 0.5));
		break;
	case 0x8E2665://boss
		temp = new Boss(Vector3(x + 0.5, 0, z + 0.5));
		break;
	case 0xFFD800:
	{
		spawnPosition = Vector2(x + 0.5, z + 0.5);
	}
	break;
	case 0x07185B:
		temp = new BouncerPickup(Vector3(x, 0, z));
		break;
	case 0x7F6A00:
	{
		float yRotation = GetYRotationFreeZone(leftBlock, rightBlock, backBlock, frontBlock);
		temp = new Door(x + 0.5, z + 0.5, yRotation);
	}
	break;
	case 0x9F52DD:
	{
		float yRotation = GetYRotationFreeZone(leftBlock, rightBlock, backBlock, frontBlock) - 90;
		temp = new GameWonDoor(x + 0.5, z + 0.5, yRotation);

		currentBlock->sprites.push_back(temp);
		temp->LinkEntityToMap(this);

		temp = new GameWonDoor(x + 0.5, z + 0.5, yRotation + 180);
	}
	break;
	case 0xFF7F7F:
	{
		float yRotation = GetYRotationFreeZone(leftBlock, rightBlock, backBlock, frontBlock);
		temp = new DynamicDoor(x + 0.5, z + 0.5, yRotation);

		currentBlock->sprites.push_back(temp);
		temp->LinkEntityToMap(this);

		temp = new DynamicDoor(x + 0.5, z + 0.5, yRotation + 180);
	}
	break;
	case 0xFF4FCA:
	{
		float yRotation = GetYRotationFreeZone(leftBlock, rightBlock, backBlock, frontBlock) - 90;
		temp = new TriggerDoor(x + 0.5, z + 0.5, yRotation);

		currentBlock->sprites.push_back(temp);
		temp->LinkEntityToMap(this);

		temp = new TriggerDoor(x + 0.5, z + 0.5, yRotation + 180);
	}
	break;
	case 0xFF6A00:
		// Lava
		if (x % 2 == 0 && z % 2 == 0)
			temp = new EntityLight(DirectX::SimpleMath::Vector3(x + 0.5f, 0.5f, z + 0.5f), DirectX::SimpleMath::Vector3(1.000f / 3.0f, 0.549f / 3.0f, 0.000f), 10.f, 0.2f);
		break;
	case 0xAFFFF9:
		temp = new ParticleEmitter(x + 0.5f, z + 0.5f);
		break;
	case 0xFF0000:

		float fx = x;
		float fz = z;

		if (dynamic_cast<SoildBlock*>(leftBlock)) {
			fx += 0.45;
		}
		else if (dynamic_cast<SoildBlock*>(rightBlock)) {
			fx -= 0.45;
		}
		else if (dynamic_cast<SoildBlock*>(frontBlock)) {
			fz += 0.45;
		}
		else if (dynamic_cast<SoildBlock*>(backBlock)) {
			fz -= 0.45;
		}
		temp = new TorchEntity(fx + 0.5f, fz + 0.5f);
		break;
	}

	if (temp != nullptr) {
		currentBlock->sprites.push_back(temp);
		temp->LinkEntityToMap(this);
	}

}

float Level::GetYRotationFreeZone(Block * leftBlock, Block * rightBlock, Block * backBlock, Block * frontBlock) const
{
	float rotation = 0.0f;
	if (!dynamic_cast<SoildBlock*>(leftBlock)) {
		rotation = 90;
	}
	else if (!dynamic_cast<SoildBlock*>(rightBlock)) {
		rotation = -90;
	}
	else if (!dynamic_cast<SoildBlock*>(backBlock)) {
		rotation = 0;
	}
	else if (!dynamic_cast<SoildBlock*>(frontBlock)) {
		rotation = 180;
	}

	return rotation;
}


//CALLUM - IMPLEMENTED METHOD FOR ENEMIES TO FIND THE PLAYER, MAY OR MAY NOT BE TEMP, YOU TELL ME
Node Level::GetPlayerNode() {
	return(GetBlockAt(player->GetPosition().x, player->GetPosition().z)->GetNode());
}

void Level::Render(float dTime) {
	if (hasCreated) {

		LevelPointLightManager::GetInstance()->Bind();
		// Render map
		RenderLevel();

		// Renders decals
		decalRenderer.Flush();

		// Renders entities
		RenderEntities(dTime);
		//Update the mesh
		if (updateMesh) {	// If update is required
			UpdateMesh();
		}

	}
}

void Level::Trigger(int id, bool pressed) {
	auto it = map.begin();
	while (it != map.end()) {
		Block* block = *it;
		if (block->GetID() == id)
			block->Trigger(pressed);

		++it;
	}
}

void Level::RenderLevel() {
	////setup shaders
	gd3dImmediateContext->VSSetShader(LevelManager::GetMyLevelManager()->vertexShader, nullptr, 0);						// Enables this vertex shader
	gd3dImmediateContext->PSSetShader(LevelManager::GetMyLevelManager()->pixelShader, nullptr, 0);						// Enables this pixel shader

	Matrix w = Matrix::CreateTranslation(Vector3(0, 0, 0));																// Set a very simple matrix for use in shader
	FX::SetPerObjConsts(gd3dImmediateContext, w);																		// Magic marks code here, I believe it just passes uniforms to the vertex and pixel shader
	InitInputAssembler(FX::GetMyFX()->mpTomInputLayout, vertexBuffer, sizeof(VertexPosNormTexColor), indexBuffer);		// More magic
	FX::GetMyFX()->PreRenderObj(gd3dImmediateContext, material, true);													// Sets up the shader to use the material
	gd3dImmediateContext->DrawIndexed(numIndices, 0, 0);																// Draws onto the screen

	gd3dImmediateContext->OMSetBlendState(0, 0, 0xffffffff);															// Unknown?
}

void Level::RenderEntities(float dTime) {
	// Updates each block
	for (Block* block : map) {
		if (block != &soildBlock) {
			RenderBlock(block, dTime);
		}
	}

	RenderBlock(&soildBlock, dTime);
}

void Level::RenderBlock(Block* block, float dTime) {
	block->Update(dTime);

	// Check whether we need to update the mesh or not
	if (!updateMesh) {
		SpecialTileBlock* specialBlock = dynamic_cast<SpecialTileBlock*>(block);
		if (specialBlock != nullptr) updateMesh |= specialBlock->requiresUpdate;
	}
	//-------------

	// Update&Renders each entity in the block
	auto it = block->sprites.begin();
	while (it != block->sprites.end()) {

		Entity* e = *it;

		if (e->HasRequestedDelete()) {
			e->UnlinkEntityFromMap();
			it = block->sprites.erase(it);
			delete e;
		}
		else
			++it;
	}

	int size = block->sprites.size();
	for (int i = 0; i < size; ++i) {
		Entity* e = block->sprites[i];

		if (dynamic_cast<Player*>(e) == nullptr) {
			e->HardUpdate(dTime);
			e->Render(dTime);
		}
	}
}

void Level::OnEnable()
{
	// All blocks except soildBlock
	for (Block* block : map) {
		if (block != &soildBlock)
		{
			for (Entity* en : block->sprites) {
				en->OnEnable();
			}
		}
	}

	// soildBlock
	for (Entity* e : soildBlock.sprites) {
		e->OnEnable();
	}
}

void Level::OnDisable()
{
	// All blocks except soildBlock
	for (Block* block : map) {
		if (block != &soildBlock)
		{
			for (Entity* en : block->sprites) {
				en->OnDisable();
			}
		}
	}

	// soildBlock
	for (Entity* e : soildBlock.sprites) {
		e->OnDisable();
	}
}

void Level::FindSpawn(int id)
{
	for (int x = 0; x < mapWidth; x++) {
		for (int z = 0; z < mapLength; z++) {
			Block* block = GetBlockAt(x, z);
			auto it = block->sprites.begin();
			while (it != block->sprites.end()) {
				Door* door = dynamic_cast<Door*>(*it);
				if (door != nullptr && block->GetID() == id) {
					spawnPosition = Vector2(x + 0.5f, z + 0.5f);
					door->wait = true;
					return;			// << I know this is terrible but is there just for temp
				}
				++it;
			}
		}
	}
}

void Level::UpdateMesh() {
	updateMesh = false;
	for (SpecialTileBlock* block : specialBlocks) {
		if (block->requiresUpdate) {
			block->requiresUpdate = false;

			int textureID = block->GetFloorTextureID() == -1 ? floorTexture : block->GetFloorTextureID();
			long hexColor = block->GetFloorTextureID() == -1 ? floorColour : block->GetFloorColour();
			if (textureID != -1) {
				const TextureRegion& tc = LevelManager::GetMyLevelManager()->GetTextureHandler()->GetTexture(textureID);

				// Floor
				VertexPosNormTexColor& v1 = posTexNormsColors[block->vertexIndex + 0];
				VertexPosNormTexColor& v2 = posTexNormsColors[block->vertexIndex + 1];
				VertexPosNormTexColor& v3 = posTexNormsColors[block->vertexIndex + 2];
				VertexPosNormTexColor& v4 = posTexNormsColors[block->vertexIndex + 3];

				// Updates texture
				v1.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV() + tc.GetMaxV());
				v2.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV());
				v3.Tex = Vector2(tc.GetMinU(), tc.GetMinV());
				v4.Tex = Vector2(tc.GetMinU(), tc.GetMinV() + tc.GetMaxV());

				// Updates color
				Vector3 rgb = HexToRGB(hexColor);
				v1.Color = Vector3(rgb.x, rgb.y, rgb.z);
				v2.Color = Vector3(rgb.x, rgb.y, rgb.z);
				v3.Color = Vector3(rgb.x, rgb.y, rgb.z);
				v4.Color = Vector3(rgb.x, rgb.y, rgb.z);
			}


		}
	}

	// Updates the mesh data
	int numVertices = posTexNormsColors.size();
	UpdateDynamicBuffer(vertexBuffer, [&](void*pDest) {
		memcpy(pDest, &posTexNormsColors[0], sizeof(VertexPosNormTexColor) * numVertices);
	});
	int numIndices = indices.size();
	UpdateDynamicBuffer(indexBuffer, [&](void*pDest) {
		memcpy(pDest, &indices[0], sizeof(UINT)*numIndices);
	});
	//------------------------
}

Block * Level::GetBlockAt(int x, int z)
{
	if (x < 0 || z < 0 || x >= mapWidth || z >= mapLength) return &soildBlock;
	return map[z * mapWidth + x];
}


void Level::CreateMap(const std::string& pngLocation) {
	Image image;
	lodepng::decode(image.data, image.width, image.height, pngLocation);
	GetPixelData(image);
	Generate(image);
	std::vector<Node*> mapNode;
	for (int i = 0; i < map.size(); i++)
	{

		mapNode.push_back((map.at(i)->GetNodePointer()));
	}
	hasCreated = true;

	//CALLUM - Added this for loop
	for (int i = 0; i < map.size(); i++)
	{
		map[i]->LinkBlockToMap(this, &mapNode, mapWidth, mapLength);
	}

}

//DEBUGGING
void Level::HighlightPath(const Queue<Node>* path) {
	for (int i = 0; i < map.size(); i++)
	{
		SpecialTileBlock* stb = dynamic_cast<SpecialTileBlock*>(map[i]);
		if (stb != nullptr)
			stb->SetFloorColor(0xCCCCCCC);
	}
	for (int i = 0; i < path->length(); i++)
	{
		SpecialTileBlock* stb = dynamic_cast<SpecialTileBlock*>(GetBlockAt(path->getObjAtPos(i).getX(), path->getObjAtPos(i).getY()));
		if (stb != nullptr)
			stb->SetFloorColor(0x55D800);
		//map[path->getObjAtPos(i).getY()  * mapWidth + path->getObjAtPos(i).getX()]
	}
}
void Level::Generate(const Image& image) {
	int width = image.width;
	int height = image.height;

	mapWidth = width;
	mapLength = height;

	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			long hexCode = image.hexdata[y * width + x];
			int alphaCode = image.alphacodedata[y * width + x];

			// BLACK = NO WALL
			if (hexCode != 0x000000) {

				// Creates the block for this current position on the map
				Block* block = CreateBlock(hexCode);
				block->SetID(alphaCode);

				long leftHexColour = image.GetPixelAt(x + 1, y);
				long rightHexColour = image.GetPixelAt(x - 1, y);
				long frontHexColour = image.GetPixelAt(x, y + 1);
				long backHexColour = image.GetPixelAt(x, y - 1);

				// Creates ajacent blocks
				Block* leftBlock = CreateBlock(leftHexColour);
				Block* rightBlock = CreateBlock(rightHexColour);
				Block* frontBlock = CreateBlock(frontHexColour);
				Block* backBlock = CreateBlock(backHexColour);

				// Used for when animating these blocks
				SpecialTileBlock* sb = dynamic_cast<SpecialTileBlock*>(block);
				if (sb != nullptr) {
					sb->vertexIndex = posTexNormsColors.size();
					specialBlocks.push_back(sb);
				}
				//-----------------

				SoildBlock* s_block = dynamic_cast<SoildBlock*>(block);
				if (s_block == nullptr) {
					// Builds map structure
					BuildFloor(*block, &posTexNormsColors, &indices, x, y);
					BuildCeil(*block, &posTexNormsColors, &indices, x, y);

					if (rightHexColour == 0x000000 || dynamic_cast<SoildBlock*>(rightBlock))
						BuildLeftWall(*block, *rightBlock, &posTexNormsColors, &indices, x, y);
					if (leftHexColour == 0x000000 || dynamic_cast<SoildBlock*>(leftBlock))
						BuildRightWall(*block, *leftBlock, &posTexNormsColors, &indices, x, y);

					if (backHexColour == 0x000000 || dynamic_cast<SoildBlock*>(backBlock))
						BuildFrontWall(*block, *backBlock, &posTexNormsColors, &indices, x, y);
					if (frontHexColour == 0x000000 || dynamic_cast<SoildBlock*>(frontBlock))
						BuildBackWall(*block, *frontBlock, &posTexNormsColors, &indices, x, y);
				}

				// Adds block to map
				block->InitialiseNode(x, y);
				map.push_back(block);

				// Creates entity which will be on this block
				CreateEntity(hexCode, x, y, block, leftBlock, rightBlock, backBlock, frontBlock);

				// Delete created blocks
				if (leftBlock != &soildBlock)delete leftBlock;
				if (rightBlock != &soildBlock)delete rightBlock;
				if (frontBlock != &soildBlock)delete frontBlock;
				if (backBlock != &soildBlock)delete backBlock;
			}
			else {
				// Adds block to map
				map.push_back(&soildBlock);
			}

		}
	}


	// Creates the renderable mesh/model
	int numVertices = posTexNormsColors.size();
	int numIndices = indices.size();

	material = MaterialExt::default;
	material.pTextureRV = LevelManager::GetMyLevelManager()->GetSpriteSheetTexture();
	//------------------------------------


	//LIAM - Creating the vertex/index buffers fail when we load back into the game after going to the main menu.
	//Since LevelManager is a singleton, it's probably holding on to some data when we go back that we need to manually release

	CreateVertexBuffer(sizeof(VertexPosNormTexColor) * numVertices, &posTexNormsColors[0], vertexBuffer, true);
	CreateIndexBuffer(sizeof(UINT)*numIndices, &indices[0], indexBuffer, true);
	this->numIndices = numIndices;

	decalRenderer.Init(LevelManager::GetMyLevelManager()->GetSpriteSheetTexture(), LevelManager::GetMyLevelManager()->GetTextureHandler());
}

void Level::BuildFloor(const Block& block, std::vector<VertexPosNormTexColor>* posTexNorms, std::vector<unsigned int>* indices, int x, int y) {
	int size = posTexNorms->size();
	int textureID = block.GetFloorTextureID() == -1 ? floorTexture : block.GetFloorTextureID();
	long hexColor = block.GetFloorTextureID() == -1 ? floorColour : block.GetFloorColour();
	if (textureID != -1) {
		const TextureRegion& tc = LevelManager::GetMyLevelManager()->GetTextureHandler()->GetTexture(textureID);

		// Picks the right vertices to render this quad
		indices->push_back(size + 0);
		indices->push_back(size + 1);
		indices->push_back(size + 2);

		indices->push_back(size + 2);
		indices->push_back(size + 3);
		indices->push_back(size + 0);
		//-------------------------

		VertexPosNormTexColor v1, v2, v3, v4;
		// Position the 4 vertices
		v1.Pos = Vector3(0 + x, 0, 0 + y);
		v2.Pos = Vector3(0 + x, 0, 1 + y);
		v3.Pos = Vector3(1 + x, 0, 1 + y);
		v4.Pos = Vector3(1 + x, 0, 0 + y);

		// Which direction they are facing
		v1.Norm = Vector3(0, 1, 0);
		v2.Norm = Vector3(0, 1, 0);
		v3.Norm = Vector3(0, 1, 0);
		v4.Norm = Vector3(0, 1, 0);

		// Their UV coords
		v1.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV() + tc.GetMaxV());
		v2.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV());
		v3.Tex = Vector2(tc.GetMinU(), tc.GetMinV());
		v4.Tex = Vector2(tc.GetMinU(), tc.GetMinV() + tc.GetMaxV());

		// Their colors
		Vector3 rgb = HexToRGB(hexColor);
		v1.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v2.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v3.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v4.Color = Vector3(rgb.x, rgb.y, rgb.z);

		// Push into array
		posTexNorms->push_back(v1);
		posTexNorms->push_back(v2);
		posTexNorms->push_back(v3);
		posTexNorms->push_back(v4);
	}
}

void Level::BuildCeil(const Block& block, std::vector<VertexPosNormTexColor>* posTexNorms, std::vector<unsigned int>* indices, int x, int y) {
	int size = posTexNorms->size();
	int textureID = block.GetCeilingTextureID() == -1 ? ceilingTexture : block.GetCeilingTextureID();
	long hexColor = block.GetCeilingTextureID() == -1 ? ceilingColour : block.GetCeilingColour();
	if (textureID != -1) {
		const TextureRegion& tc = LevelManager::GetMyLevelManager()->GetTextureHandler()->GetTexture(textureID);

		indices->push_back(size + 2);
		indices->push_back(size + 1);
		indices->push_back(size + 0);

		indices->push_back(size + 0);
		indices->push_back(size + 3);
		indices->push_back(size + 2);

		VertexPosNormTexColor v1, v2, v3, v4;
		v1.Pos = Vector3(0 + x, 1, 0 + y);
		v2.Pos = Vector3(0 + x, 1, 1 + y);
		v3.Pos = Vector3(1 + x, 1, 1 + y);
		v4.Pos = Vector3(1 + x, 1, 0 + y);

		v1.Norm = Vector3(0, -1, 0);
		v2.Norm = Vector3(0, -1, 0);
		v3.Norm = Vector3(0, -1, 0);
		v4.Norm = Vector3(0, -1, 0);

		v1.Tex = Vector2(tc.GetMinU(), tc.GetMinV());
		v2.Tex = Vector2(tc.GetMinU(), tc.GetMinV() + tc.GetMaxV());
		v3.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV() + tc.GetMaxV());
		v4.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV());

		Vector3 rgb = HexToRGB(hexColor);
		v1.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v2.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v3.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v4.Color = Vector3(rgb.x, rgb.y, rgb.z);

		posTexNorms->push_back(v1);
		posTexNorms->push_back(v2);
		posTexNorms->push_back(v3);
		posTexNorms->push_back(v4);
	}
}

void Level::BuildLeftWall(const Block& block, const Block& leftBlock, std::vector<VertexPosNormTexColor>* posTexNorms, std::vector<unsigned int>* indices, int x, int y) {
	int size = posTexNorms->size();
	int textureID = leftBlock.HasCollision() ? leftBlock.GetWallTextureID() : block.GetWallTextureID();
	long hexColor = leftBlock.GetCeilingTextureID() == -1 ? leftBlock.GetWallColour() : block.GetWallColour();
	hexColor = textureID == -1 ? wallColour : hexColor;
	textureID = textureID == -1 ? wallTexture : textureID;

	if (textureID != -1) {
		const TextureRegion& tc = LevelManager::GetMyLevelManager()->GetTextureHandler()->GetTexture(textureID);

		indices->push_back(size + 2);
		indices->push_back(size + 1);
		indices->push_back(size + 0);

		indices->push_back(size + 0);
		indices->push_back(size + 3);
		indices->push_back(size + 2);

		VertexPosNormTexColor v1, v2, v3, v4;
		v1.Pos = Vector3(0 + x, 0, 0 + y);
		v2.Pos = Vector3(0 + x, 0, 1 + y);
		v3.Pos = Vector3(0 + x, 1, 1 + y);
		v4.Pos = Vector3(0 + x, 1, 0 + y);

		v1.Norm = Vector3(1, 0, 0);
		v2.Norm = Vector3(1, 0, 0);
		v3.Norm = Vector3(1, 0, 0);
		v4.Norm = Vector3(1, 0, 0);

		v1.Tex = Vector2(tc.GetMinU(), tc.GetMinV() + tc.GetMaxV());
		v2.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV() + tc.GetMaxV());
		v3.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV());
		v4.Tex = Vector2(tc.GetMinU(), tc.GetMinV());

		Vector3 rgb = HexToRGB(hexColor);
		v1.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v2.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v3.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v4.Color = Vector3(rgb.x, rgb.y, rgb.z);

		posTexNorms->push_back(v1);
		posTexNorms->push_back(v2);
		posTexNorms->push_back(v3);
		posTexNorms->push_back(v4);
	}
}

void Level::BuildRightWall(const Block& block, const Block& rightBlock, std::vector<VertexPosNormTexColor>* posTexNorms, std::vector<unsigned int>* indices, int x, int y) {
	int size = posTexNorms->size();

	int textureID = rightBlock.HasCollision() ? rightBlock.GetWallTextureID() : block.GetWallTextureID();
	long hexColor = rightBlock.GetCeilingTextureID() == -1 ? rightBlock.GetWallColour() : block.GetWallColour();
	hexColor = textureID == -1 ? wallColour : hexColor;
	textureID = textureID == -1 ? wallTexture : textureID;
	if (textureID != -1) {
		const TextureRegion& tc = LevelManager::GetMyLevelManager()->GetTextureHandler()->GetTexture(textureID);

		indices->push_back(size + 0);
		indices->push_back(size + 1);
		indices->push_back(size + 2);

		indices->push_back(size + 2);
		indices->push_back(size + 3);
		indices->push_back(size + 0);

		VertexPosNormTexColor v1, v2, v3, v4;
		v1.Pos = Vector3(1 + x, 0, 0 + y);
		v2.Pos = Vector3(1 + x, 0, 1 + y);
		v3.Pos = Vector3(1 + x, 1, 1 + y);
		v4.Pos = Vector3(1 + x, 1, 0 + y);

		v1.Norm = Vector3(-1, 0, 0);
		v2.Norm = Vector3(-1, 0, 0);
		v3.Norm = Vector3(-1, 0, 0);
		v4.Norm = Vector3(-1, 0, 0);

		v1.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV() + tc.GetMaxV());
		v2.Tex = Vector2(tc.GetMinU(), tc.GetMinV() + tc.GetMaxV());
		v3.Tex = Vector2(tc.GetMinU(), tc.GetMinV());
		v4.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV());

		Vector3 rgb = HexToRGB(hexColor);
		v1.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v2.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v3.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v4.Color = Vector3(rgb.x, rgb.y, rgb.z);

		posTexNorms->push_back(v1);
		posTexNorms->push_back(v2);
		posTexNorms->push_back(v3);
		posTexNorms->push_back(v4);
	}
}

void Level::BuildFrontWall(const Block& block, const Block& frontBlock, std::vector<VertexPosNormTexColor>* posTexNorms, std::vector<unsigned int>* indices, int x, int y) {
	int size = posTexNorms->size();

	int textureID = frontBlock.HasCollision() ? frontBlock.GetWallTextureID() : block.GetWallTextureID();
	long hexColor = frontBlock.GetCeilingTextureID() == -1 ? frontBlock.GetWallColour() : block.GetWallColour();
	hexColor = textureID == -1 ? wallColour : hexColor;
	textureID = textureID == -1 ? wallTexture : textureID;
	if (textureID != -1) {
		const TextureRegion& tc = LevelManager::GetMyLevelManager()->GetTextureHandler()->GetTexture(textureID);

		indices->push_back(size + 2);
		indices->push_back(size + 1);
		indices->push_back(size + 0);

		indices->push_back(size + 0);
		indices->push_back(size + 3);
		indices->push_back(size + 2);

		VertexPosNormTexColor v1, v2, v3, v4;
		v1.Pos = Vector3(0 + x, 0, 0 + y);
		v2.Pos = Vector3(0 + x, 1, 0 + y);
		v3.Pos = Vector3(1 + x, 1, 0 + y);
		v4.Pos = Vector3(1 + x, 0, 0 + y);

		v1.Norm = Vector3(0, 0, 1);
		v2.Norm = Vector3(0, 0, 1);
		v3.Norm = Vector3(0, 0, 1);
		v4.Norm = Vector3(0, 0, 1);

		v1.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV() + tc.GetMaxV());
		v2.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV());
		v3.Tex = Vector2(tc.GetMinU(), tc.GetMinV());
		v4.Tex = Vector2(tc.GetMinU(), tc.GetMinV() + tc.GetMaxV());

		Vector3 rgb = HexToRGB(hexColor);
		v1.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v2.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v3.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v4.Color = Vector3(rgb.x, rgb.y, rgb.z);

		posTexNorms->push_back(v1);
		posTexNorms->push_back(v2);
		posTexNorms->push_back(v3);
		posTexNorms->push_back(v4);
	}
}

void Level::BuildBackWall(const Block& block, const Block& backBlock, std::vector<VertexPosNormTexColor>* posTexNorms, std::vector<unsigned int>* indices, int x, int y) {
	int size = posTexNorms->size();

	int textureID = backBlock.HasCollision() ? backBlock.GetWallTextureID() : block.GetWallTextureID();
	long hexColor = backBlock.GetCeilingTextureID() == -1 ? backBlock.GetWallColour() : block.GetWallColour();
	hexColor = textureID == -1 ? wallColour : hexColor;
	textureID = textureID == -1 ? wallTexture : textureID;
	if (textureID != -1) {
		const TextureRegion& tc = LevelManager::GetMyLevelManager()->GetTextureHandler()->GetTexture(textureID);

		indices->push_back(size + 0);
		indices->push_back(size + 1);
		indices->push_back(size + 2);

		indices->push_back(size + 2);
		indices->push_back(size + 3);
		indices->push_back(size + 0);

		VertexPosNormTexColor v1, v2, v3, v4;
		v1.Pos = Vector3(0 + x, 0, 1 + y);
		v2.Pos = Vector3(0 + x, 1, 1 + y);
		v3.Pos = Vector3(1 + x, 1, 1 + y);
		v4.Pos = Vector3(1 + x, 0, 1 + y);

		v1.Norm = Vector3(0, 0, -1);
		v2.Norm = Vector3(0, 0, -1);
		v3.Norm = Vector3(0, 0, -1);
		v4.Norm = Vector3(0, 0, -1);

		v1.Tex = Vector2(tc.GetMinU(), tc.GetMinV() + tc.GetMaxV());
		v2.Tex = Vector2(tc.GetMinU(), tc.GetMinV());
		v3.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV());
		v4.Tex = Vector2(tc.GetMinU() + tc.GetMaxU(), tc.GetMinV() + tc.GetMaxV());

		Vector3 rgb = HexToRGB(hexColor);
		v1.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v2.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v3.Color = Vector3(rgb.x, rgb.y, rgb.z);
		v4.Color = Vector3(rgb.x, rgb.y, rgb.z);

		posTexNorms->push_back(v1);
		posTexNorms->push_back(v2);
		posTexNorms->push_back(v3);
		posTexNorms->push_back(v4);
	}
}

DirectX::SimpleMath::Vector3 Level::HexToRGB(long hexCode) const
{
	DirectX::SimpleMath::Vector3 rgbColor;
	rgbColor.x = ((hexCode >> 16) & 0xFF) / 255.0f;  // Extract the RR byte
	rgbColor.y = ((hexCode >> 8) & 0xFF) / 255.0f;   // Extract the GG byte
	rgbColor.z = ((hexCode) & 0xFF) / 255.0f;        // Extract the BB byte
	return rgbColor;
}

void Level::GetPixelData(Image& image) {
	int currentPixel = 0;
	for (int y = 0; y < image.height; y++)
	{
		for (int x = 0; x < image.width; x++)
		{
			int r = image.data[(y * image.width * 4) + (x * 4 + 0)];
			int g = image.data[(y * image.width * 4) + (x * 4 + 1)];
			int b = image.data[(y * image.width * 4) + (x * 4 + 2)];
			int a = image.data[(y * image.width * 4) + (x * 4 + 3)];
			long hexCode = CreateRGB(r, g, b);

			image.hexdata.push_back(hexCode);
			image.alphacodedata.push_back(255 - a);
		}
	}
}


unsigned long Level::CreateRGB(int r, int g, int b) const
{
	return ((r & 0xff) << 16) + ((g & 0xff) << 8)
		+ (b & 0xff);
}

//VertexPosNormTex Level::BuildPart(const Vector3& position, const Vector3& normal, const Vector2& textureCoord)
//{
//	VertexPosNormTex vertex;
//	vertex.Pos = position;
//	vertex.Norm = normal;
//	vertex.Tex = textureCoord;
//	return vertex;
//}
