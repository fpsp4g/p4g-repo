#include "LeaderboardMenu.h"

LeaderboardMenu::LeaderboardMenu()
	: Menu("Leaderboard", 2, "ui/background.dds", 1.f)
{
	InitialiseButtons();
}

LeaderboardMenu::~LeaderboardMenu()
{ }

void LeaderboardMenu::RenderBackground(int w, int h)
{
	Vector2 windowCenter(w / 2, h / 2);
	FX::GetMyFX()->mpSpriteB->Draw(FX::GetMyFX()->mCache.Get(backgroundTexIdx).pTex, (windowCenter - backgroundCenter), nullptr, Colours::White, 0, Vector2(0, 0), Vector2(bGScale));
}
void LeaderboardMenu::RenderTitle(int w, int h)
{
	// Box
	Vector2 boxPos;
	boxPos.x = (w / 2);
	boxPos.y = 50 + titleCenter.y;
	RenderBox(boxPos, 7, 0.1);

	// Title
	Vector2 textPos = Vector2((w / 2 - titleCenter.x), 50);
	FX::GetMyFX()->mpFont->DrawString(FX::GetMyFX()->mpSpriteB, title.str().c_str(), textPos, Colors::White, 0, Vector2(0, 0), ttlScale);
}
void LeaderboardMenu::RenderButtons(int w, int h)
{
	Vector2 pos;
	pos.x = w / 2;
	pos.y = h - buttons[0].GetDimensions().y;
	buttons[0].Render(pos);
}

void LeaderboardMenu::InitialiseButtons()
{
	// Exit button
	Button btnExit("Return to menu", buttonTexIdx, 0.1f, 1.f);
	buttons.push_back(btnExit); // buttons[0] = Exit
}