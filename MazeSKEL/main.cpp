#include "WindowUtils.h"
#include "D3D.h"
#include "FX.h"
#include "Mesh.h"
#include "Input.h"
#include "File.h"
#include "UserFolder.h"
#include "AudioMgrFMOD.h"
#include "AudioMgr.h"
#include "Game.h"
#include "GameModeManager.h"


using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

Game gGame;


void Update(float dTime)
{
	GetIAudioMgr()->Update();
	gGame.Update(dTime);
}

void Render(float dTime)
{
	gGame.Render(dTime);
}

void OnResize(int screenWidth, int screenHeight)
{
	gGame.OnResize(screenWidth, screenHeight);
}


LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_INPUT:
		GetMouseAndKeys()->MessageEvent((HRAWINPUT)lParam);
		break;
	}

	return gGame.WindowsMssgHandler(hwnd, msg, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
	PSTR cmdLine, int showCmd)
{
	SeedRandom();
	File::initialiseSystem();
	USER::SetUserDataFolder("MyGame");

	if (!InitMainWindow(1280, 720, hInstance, "Dungeon Hunt", MainWndProc))
		assert(false);

	if (!InitDirect3D(OnResize))
		assert(false);

	gGame.Initialise();

	//startup
	new FX::MyFX(gd3dDevice);
	new MouseAndKeys;
	GetMouseAndKeys()->Initialise(GetMainWnd());

	new Gamepad;
	GetGamepad()->Initialise();

	new AudioMgrFMOD;
	GetIAudioMgr()->Initialise();


	GameModeManager* modeManager = GameModeManager::GetInstance();
	modeManager->SetGame(&gGame);
	modeManager->SwapMode(0); //Title screen

	Run(Update, Render);

	//shut down
	gGame.Release();
	delete GetMouseAndKeys();
	delete GetGamepad();
	delete FX::GetMyFX();
	delete GetIAudioMgr();


	ReleaseD3D();

	return 0;
}
