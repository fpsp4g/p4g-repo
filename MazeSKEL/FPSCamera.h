#ifndef FPSCAMERA_H
#define FPSCAMERA_H

#include "d3d11.h"
#include "SimpleMath.h"
/*
First person camera, forwards backwards left right and rotate
*/
class FPSCamera
{
public:
	enum { UNLOCK = 999999 };

	FPSCamera():mCamPos(nullptr){}

	//set initial camera position and the matrix to modify
	void Initialise(DirectX::SimpleMath::Vector3* pos, const DirectX::SimpleMath::Vector3& tgt, DirectX::SimpleMath::Matrix& viewSpaceTfm);
	
	DirectX::SimpleMath::Vector3& GetPos() { return *mCamPos; }

	void UpdateViewMatrix(float dTime);

	//rotation can be driven by mouse x=yaw and y=pitch
	void Rotate(float dTime, float _yaw, float _pitch, float _roll, bool controllerEnabled);

	//stop camera moving in a certain axis, e.g. a FPS camera that always stays a fixed height in Y
	void LockMovementAxis(float x = UNLOCK, float y = UNLOCK, float z = UNLOCK) 
	{
		mLockAxis = DirectX::SimpleMath::Vector3(x, y, z);
	}

	const DirectX::SimpleMath::Matrix& GetMatrix() const { return *mpViewSpaceTfm; };

	const float GetYaw() const		{ return yaw; };
	const float GetPitch() const	{ return pitch; };
	const float GetRoll() const		{ return roll; };
	void SetYaw(float value)		{ yaw = value; };
	void SetPitch(float value)		{ pitch = value; };
	void SetRoll(float value)		{ roll = value; };

	const float GetCamRotateSpeed() const	{ return camRotateSpeed; };
	void SetCamRotateSpeed(float value)		{ camRotateSpeed = value; };
	
private:
	DirectX::SimpleMath::Vector3 mLockAxis{ UNLOCK, UNLOCK, UNLOCK };

	//the camera matrix to modify
	DirectX::SimpleMath::Matrix* mpViewSpaceTfm;

	//camera position
	DirectX::SimpleMath::Vector3* mCamPos;
	DirectX::SimpleMath::Vector3 mTargetPos;

	//camera rotation
	float camRotateSpeed = 2.5f;
	float yaw = 0, pitch = 0, roll = 0;
};

#endif