/*******************************************************
* Copyright (C) 2018 Thomas Dunn tom1998dunn@hotmail.com
*
* This file is part of P4G Assignment.
*
* This code can not be copied and/or distributed without the express
* permission of Thomas Dunn
*******************************************************/

#ifndef TEXTUREREGIONHANDLERH
#define TEXTUREREGIONHANDLERH

#include "TextureRegion.h"
#include <vector>
#include "FX.h"
#include "D3D.h"

class TextureRegionHandler {

public:
	TextureRegionHandler(ID3D11ShaderResourceView*,int split = 16);
	const TextureRegion& GetTexture(int index) const;
private:
	std::vector<TextureRegion> regions;
	ID3D11ShaderResourceView* texture;
};

#endif