#include "TriggerDoorBlock.h"
#include "TriggerDoor.h"

void TriggerDoorBlock::Trigger(bool pressed)
{
	auto it = sprites.begin();
	while (it != sprites.end())
	{
		(*it)->Trigger(pressed);
		++it;
	}
}
