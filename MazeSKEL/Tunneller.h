#ifndef TUNNELLER_H
#define TUNNELLER_H

#include "TurretEnemy.h"
#include "TunnellerDust.h"
class Tunneller : public TurretEnemy
{
public:
	Tunneller(Vector3 pos, float dTime, Level* level);
	virtual ~Tunneller();
	virtual void IdleBehaviour(float);
	virtual void AttackBehaviour(float);
private:
	Vector3 endLocation; //the place that the tunneller wants to move to
	TunnellerDust *dust;
	unsigned int digSound;
	float attackTimer = 0;
	const float maxAttackTime = 1.5f;	//base value for the delay between attacks, can be changed at will, and it may be worth changing it on a per enemy basis
};



#endif // !TUNNELLER_H

