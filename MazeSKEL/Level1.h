#ifndef LEVEL1H
#define LEVEL1H

#include "Level.h"

class Level1 : public Level {

public:
	void Init();
	virtual void SwitchLevel(int id);

};

#endif