#ifndef MENU_GAMEOVER_H
#define MENU_GAMEOVER_H

#include "Menu.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

class GameEndMenu : public Menu
{
public:
	GameEndMenu();
	virtual ~GameEndMenu();

	virtual void RenderBackground(int w, int h);
	virtual void RenderTitle(int w, int h);
	virtual void RenderButtons(int w, int h);

private:
	virtual void InitialiseButtons();

};

#endif