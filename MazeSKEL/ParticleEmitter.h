#ifndef PARTICLEEMITTERH
#define PARTICLEEMITTERH

#include "Particle.h"
#include <vector>
#include <random>

class ParticleEmitter : public Entity {

public:
	ParticleEmitter(float x,float z);
	~ParticleEmitter();
	virtual void Render(float dTime);		// This will handle updating and rendering the particles
	virtual void Update(float dTime);		// This will be a custom method for each particle system to handle how a particle should spawn
protected:
	void AddParticle(Particle* particle);
	float time{ 0 };
private:
	std::vector<Particle*> particles;

};

#endif