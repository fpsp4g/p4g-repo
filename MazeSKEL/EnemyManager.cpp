#include "EnemyManager.h"


//void EnemyManager::CheckForDeadEnemies() 
//{
//	for (int i = 0; i < Enemies.size(); i++)
//	{
//		if (Enemies.at(i)->IsDead()) 
//		{
//			delete(Enemies.at(i));
//			Enemies.erase(Enemies.begin() + i);
//		}
//	}
//}

Enemy* EnemyManager::SpawnEnemy(Vector3 pos) 
{
	Enemy* enemy = new MeleeEnemy(pos);
	Enemies.push_back(enemy);
	return enemy;
}

void EnemyManager::RenderEnemies(float dTime)
{
	for (int i = 0; i < Enemies.size(); i++)
	{
		Enemies.at(i)->Render(dTime);
	}
}
void EnemyManager::UpdateEnemies(float dTime)
{
	for (int i = 0; i < Enemies.size(); i++)
	{
		Enemies.at(i)->HardUpdate(dTime);
	}
}

void EnemyManager::Release() {
	for (int i = 0; i < Enemies.size(); i++)
	{
		Enemies.at(i)->DestroySelf();
		Enemies.at(i) = nullptr;
	}
	Enemies.clear();
	EnemyManager* temp = instance;
	instance = nullptr;
	delete(temp);
	
}