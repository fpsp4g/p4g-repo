#include "LevelPointLightManager.h"



LevelPointLightManager::LevelPointLightManager()
{
}

void LevelPointLightManager::Activate(float disToPlayer, DirectX::SimpleMath::Vector3 position, DirectX::SimpleMath::Vector3 color,float range,float atten)
{
	// Can we manage any more lights
	if (managedLights < MAX_LIGHTS_MANAGED) {
		// Check if its in with range
		if (disToPlayer <= MAX_DISTANCE) {

			Data data{disToPlayer, position, color,range,atten };
			managedData.push_back(data);
			managedLights++;
		}

	}
}

void LevelPointLightManager::Bind()
{
	SortLights();
	BindLights();

	managedLights = 0;
	managedData.clear();
}

void LevelPointLightManager::BindLights()
{
	int max_lights = managedLights < MAX_LIGHTS ? managedLights : MAX_LIGHTS;

	int lidx = 0;
	// Adds lights 
	for (; lidx < max_lights; ++lidx)
	{
		int lightIndex = lidx + 1;
		FX::SetupPointLight(lightIndex, true, managedData[lidx].lightPosition, managedData[lidx].lightColour,DirectX::SimpleMath::Vector3()
		,DirectX::SimpleMath::Vector3(), managedData[lidx].range, managedData[lidx].atten);
	}

	// Remove previous frames lights
	for (; lidx < MAX_LIGHTS; ++lidx) {
		int lightIndex = lidx + 1;
		FX::SetupPointLight(lightIndex, false, DirectX::SimpleMath::Vector3(), DirectX::SimpleMath::Vector3());
	}
}

// Ensure that the front lights are the closest
void LevelPointLightManager::SortLights()
{
	std::sort(managedData.begin(), managedData.end());
}
