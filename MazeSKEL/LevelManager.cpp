/*******************************************************
* Copyright (C) 2018 Thomas Dunn tom1998dunn@hotmail.com
*
* This file is part of P4G Assignment.
*
* This code can not be copied and/or distributed without the express
* permission of Thomas Dunn
*******************************************************/

#include "LevelManager.h"

static LevelManager* instance;

LevelManager::LevelManager() : currentLevel(nullptr) {
	instance = this;
}

void LevelManager::Init(ID3D11ShaderResourceView* spriteSheetTexture) {
	this->spriteSheetTexture = spriteSheetTexture;
	textureRegionHandler = new TextureRegionHandler(spriteSheetTexture);

	vertexShader = FX::GetMyFX()->mpTomVS;
	pixelShader = FX::GetMyFX()->mpTomPS;

	player = new Player();

	LoadLevel("Level2");
}

void LevelManager::Render(float dTime) {

	player->HardUpdate(dTime);

	if (currentLevel != nullptr)
		currentLevel->Render(dTime);

	//player->Render(dTime);
}

void LevelManager::LoadLevel(const std::string& name) {

	// Check if level is already in memory
	std::map<std::string, Level*>::iterator it = loadedLevels.find(name);
	if (it != loadedLevels.end()) {
		if (currentLevel != nullptr) {
			player->UnlinkEntityFromMap();
			currentLevel->OnDisable();
		}
		// We have found the level
		Level* level = loadedLevels[name];
		currentLevel = level;

	}
	else {
		Level* level(nullptr);
		// The level needs loading
		if (name == "TestLevel") {
			level = new TestLevel();
		}
		else if (name == "Level1") {
			level = new Level1();
		}
		else if (name == "Level2") {
			level = new Level2();
		}

		if (level != nullptr) {
			loadedLevels.insert(std::pair<std::string, Level*>(name, level));
			level->SetPlayer(player);
			level->Init();
			const DirectX::SimpleMath::Vector2& spawn = level->GetSpawnPosition();
			player->SetPosition(DirectX::SimpleMath::Vector3(spawn.x, 0.5f, spawn.y));
			currentLevel = level;
		}
	}

	if (currentLevel != nullptr) {
		player->LinkEntityToMap(currentLevel);
		currentLevel->OnEnable();
	}

}

void LevelManager::SwitchLevel(const std::string & level, int id)
{
	player->OnDisable();

	LoadLevel(level);
	currentLevel->FindSpawn(id);

	const DirectX::SimpleMath::Vector2& spawn = currentLevel->GetSpawnPosition();
	player->Reset(DirectX::SimpleMath::Vector3(spawn.x, 0.5f, spawn.y));
	player->OnEnable();

}

void LevelManager::ClearCache() {
	std::map<std::string, Level*>::iterator it = loadedLevels.begin();
	while (it != loadedLevels.end())
	{
		delete it->second;
		it++;
	}
	loadedLevels.clear();
}

ID3D11ShaderResourceView * LevelManager::GetSpriteSheetTexture()
{
	return spriteSheetTexture;
}

TextureRegionHandler * LevelManager::GetTextureHandler()
{
	return textureRegionHandler;
}

LevelManager * LevelManager::GetMyLevelManager()
{
	return instance;
}

LevelManager::~LevelManager() {
	ClearCache();

	delete player;
	delete textureRegionHandler;
}