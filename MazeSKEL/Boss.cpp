#include "Boss.h"
#include "Leaderboard.h"

Boss::Boss(Vector3 pos) : RangedEnemy(pos, "TurretDrone", 2000)
{
}
Boss::~Boss()
{

}


void Boss::ChaseBahaviour(float dTime)
{
	if (tunnellerTimer > 0)
	{
		tunnellerTimer -= dTime;
		//move towards the player (the boss can always see the player)
		MoveTowards(Vector3(level->GetPlayerNode().getX() + 0.5, GetPosition().y, level->GetPlayerNode().getY() + 0.5), dTime);
		if (attackTimer <= 0)
		{
			attackTimer = maxAttackTime;
			state = EnemyState::ATTACK;
		}
		else
		{
			attackTimer -= dTime;
		}
		
	}
	else
	{
		tunnellerTimer = maxTunnellerTimer;
		//spawn a tunneller
		Tunneller* pTun = new Tunneller(Vector3(position.x, 0, position.z), dTime, level);
		level->GetBlockAt(-1,-1)->sprites.push_back(pTun);
		pTun->LinkEntityToMap(level);
	}
}

void Boss::Die()
{
	if (GetIAudioMgr()->GetSfxMgr()->IsPlaying(deathSound) == false)
		GetIAudioMgr()->GetSfxMgr()->Play("145788__cyberios__small-explosion", false, false, &deathSound, 0.5 / (level->GetPlayer()->GetPosition() - position).Length());
	else
		GetIAudioMgr()->GetSfxMgr()->Stop(deathSound);

	Leaderboard::GetInstance()->GetCurrentScore()->ChangeScore(50);
	DestroySelf();

	// Adds exploding particles
	ExplodeEmitter* pSystem = new ExplodeEmitter(position.x, position.z);
	level->GetBlockAt(-1,-1)->sprites.push_back(pSystem);
	pSystem->LinkEntityToMap(level);

	level->Trigger(0, true);
}
