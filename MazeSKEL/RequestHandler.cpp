#include "RequestHandler.h"
RequestHandler* RequestHandler::instance(nullptr);

void RequestHandler::AddRequestToQueue(Node startNode, Node endNode, Queue<Node>* path, bool* pathFound)
{
	PathRequest request{ startNode,endNode,path, pathFound };
	requests.addToEnd(request);
}

//Get pathfinder to process our next request
void RequestHandler::ProcessNextRequest()
{
	if (requests.length() != 0) {
		Pathfinder::GetInstance()->FindPath(requests.first().startNode, requests.first().endNode, *requests.first().pathToUpDate, requests.first().pathFound);
		requests.removeFirst();
	}
	if (lineRequests.length() != 0)
	{
		Pathfinder::GetInstance()->DrawStraightLine(lineRequests.first().startNode, lineRequests.first().endNode, lineRequests.first().canSearch, lineRequests.first().lineFound);
		lineRequests.removeFirst();
	}
}

void RequestHandler::release() {
	delete(Pathfinder::GetInstance());
	delete(RequestHandler::GetInstance());
}
