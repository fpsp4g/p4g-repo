#include "DecalEntity.h"

#include "Level.h"

void DecalEntity::Render(float dTime) {
	level->GetDecalRenderer()->Render(this);
}
void DecalEntity::Update(float dTime) {

}

void DecalEntity::SetTint(const DirectX::SimpleMath::Vector3 & tint)
{
	this->tint = tint;
}
