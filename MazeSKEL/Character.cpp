#include "Character.h"

Character::Character(int maxHpc)
	: maxHp(maxHpc),hp(maxHpc)
{ }

void Character::TakeDamage(int damage)
{
	if (visible && hp > 0) {
		hp -= damage;
		if (hp <= 0)
			Die();
	}
}
void Character::Heal(int amount)
{
	hp += amount;
	if (hp > maxHp)
		hp = maxHp;
}