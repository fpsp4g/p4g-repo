#include "ExplodeEmitter.h"

ExplodeEmitter::ExplodeEmitter(float x, float z) : ParticleEmitter(x,z)
{
}

void ExplodeEmitter::Update(float dTime)
{
	time += dTime;
	deathTime -= dTime;

	// Adds particles
	if (time >= 0.005f) {
		time = 0;

		float spread = 2.5f;
		float randomX = ((((double)rand() / (RAND_MAX)) * 2) - 1) * spread;
		float randomZ = ((((double)rand() / (RAND_MAX)) * 2) - 1) * spread;
		float randomY = ((((double)rand() / (RAND_MAX)) * 2) - 1) * spread;
		float greenComponent = (((double)rand() / (RAND_MAX)));
		Particle* p = new Particle(DirectX::SimpleMath::Vector3(position.x,position.y + 0.5f,position.z), DirectX::SimpleMath::Vector3(randomX, randomY, randomZ), 0.2f);
		p->SetTint(DirectX::SimpleMath::Vector3(1, greenComponent, 0));
		AddParticle(p);

	}

	// Destory system
	if (deathTime <= 0)
		DestroySelf();

}
