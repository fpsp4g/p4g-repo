#include "DynamicDoor.h"
#include "Level.h"

DynamicDoor::DynamicDoor(float x, float z, float yRotation) :DecalEntity(x, z), isOpening(false), time(0), MIN_TEXTURE(48), MAX_TEXTURE(48 + 6), DISTANCE_TO_OPEN(10.0f) {
	rotation.y = yRotation;
	textureIndex = MIN_TEXTURE;
	isTriggerCollision = false;
}

void DynamicDoor::Update(float dTime)
{
	using namespace DirectX::SimpleMath;
	bool wasOpen = isOpening;
	isOpening = false;
	// list of blocks we will need to check for Entity distance
	Block* blockCollisionList[] =
	{
		level->GetBlockAt(position.x,position.z),
		level->GetBlockAt(position.x - 1,position.z),
		level->GetBlockAt(position.x + 1,position.z),
		level->GetBlockAt(position.x,position.z + 1),
		level->GetBlockAt(position.x,position.z - 1),
		level->GetBlockAt(position.x + 1,position.z + 1),
		level->GetBlockAt(position.x + 1,position.z - 1),
		level->GetBlockAt(position.x - 1,position.z + 1),
		level->GetBlockAt(position.x - 1,position.z - 1),
	};

	// Quickly finds if any entities nearby are close enough to open the door
	[&] {
		for (Block* block : blockCollisionList) {
			for (Entity* enBlock : block->entities) {
				if (dynamic_cast<Character*>(enBlock)) {
					float distance = Vector3::Distance(enBlock->GetPosition(), GetPosition());
					isOpening = distance <= DISTANCE_TO_OPEN;

				}
				if (isOpening) {
					if (!wasOpen) {
						if (GetIAudioMgr()->GetSfxMgr()->IsPlaying(swooshSound) == false)
							GetIAudioMgr()->GetSfxMgr()->Play("55826__sergenious__dooropen1", false, false, &swooshSound, 0.5 / (level->GetPlayer()->GetPosition() - position).Length());
						else
							GetIAudioMgr()->GetSfxMgr()->Stop(swooshSound);
					}
					return;
				}
			}
		}
	}();

	/////////////////////////////////////////////////
	UpdateAnimation(dTime);
}

void DynamicDoor::UpdateAnimation(float dTime)
{
	time += dTime;
	if (time > 0.02f) {
		time = 0;
		//////////////////////////////////
		if (isOpening) {
			// Is opening door
			if (textureIndex < MAX_TEXTURE)
				textureIndex++;
		}
		else {
			// Is closing door
			if (textureIndex > MIN_TEXTURE)
				textureIndex--;
		}



		isTriggerCollision = textureIndex >= MIN_TEXTURE + 3;


		///////////////////////////////////
	}
}
