#include "Particle.h"
#include "Level.h"

Particle::Particle(DirectX::SimpleMath::Vector3 position_, DirectX::SimpleMath::Vector3 velocity_, float timeTillDestoryInSecs) : velocity(velocity_), destroyTimeInSeconds(timeTillDestoryInSecs)
{
	position = position_;
	textureIndex = 98;
	hasLevelCollision = false;
	isTriggerCollision = true;
}

void Particle::Update(float dTime)
{
	if (time >= destroyTimeInSeconds)
		DestroySelf();

	time += dTime;
	position += velocity * dTime;
}
