#include "Entity.h"
#include "Block.h"
#include "SpecialTileBlock.h"
#include "Level.h"
#include "Projectile.h"
#include "Pickup.h"
#include <algorithm>
void Entity::HardUpdate(float dTime)
{

	if (!level ) {
		return; //Level or player has not been instantiated yet so dont bother
	}
	// Update all custom game logic
	Update(dTime);

	// Handles level collision
	HandleLevelCollision();
	// Handles entity collision
	HandleEntityCollision();
	if (!level)//some the game can end in these lines of code, so if it has, stop doing what you're doing
	{
		return;
	}
	// Removes from previous space
	std::vector<Entity*>* entities = &level->GetBlockAt(static_cast<int>(lastPosition.x), static_cast<int>(lastPosition.z))->entities;
	entities->erase(std::remove(entities->begin(), entities->end(), this), entities->end());
	// Adds to current space
	level->GetBlockAt(static_cast<int>(position.x), static_cast<int>(position.z))->entities.push_back(this);
	// Update the postion
	lastPosition = position;
}

void Entity::OnEnable()
{
}

void Entity::OnDisable()
{
}


void Entity::Trigger(bool pressed)
{
}

void Entity::HandleLevelCollision()
{
	if (hasLevelCollision) {
		DirectX::BoundingBox bb = GetBoundingBox();
		// Checks if has collided in X direction
		bool hasCollidedX = false;
		hasCollidedX = level->GetBlockAt(static_cast<int>(position.x - bb.Extents.x), static_cast<int>(lastPosition.z))->HasCollision();
		hasCollidedX |= level->GetBlockAt(static_cast<int>(position.x + bb.Extents.x), static_cast<int>(lastPosition.z))->HasCollision();
		hasCollidedX |= level->GetBlockAt(static_cast<int>(position.x), static_cast<int>(lastPosition.z - bb.Extents.z))->HasCollision();
		hasCollidedX |= level->GetBlockAt(static_cast<int>(position.x), static_cast<int>(lastPosition.z + bb.Extents.z))->HasCollision();

		if (hasCollidedX)
			position.x = lastPosition.x;

		// Check if has collided in Z direction
		bool hasCollidedZ = false;
		hasCollidedZ = level->GetBlockAt(static_cast<int>(lastPosition.x - bb.Extents.x), static_cast<int>(position.z))->HasCollision();
		hasCollidedZ |= level->GetBlockAt(static_cast<int>(lastPosition.x + bb.Extents.x), static_cast<int>(position.z))->HasCollision();
		hasCollidedZ |= level->GetBlockAt(static_cast<int>(lastPosition.x), static_cast<int>(position.z - bb.Extents.z))->HasCollision();
		hasCollidedZ |= level->GetBlockAt(static_cast<int>(lastPosition.x), static_cast<int>(position.z + bb.Extents.z))->HasCollision();

		if (hasCollidedZ)
			position.z = lastPosition.z;

		// Check if collided in the corner parts of the blocks
		if (!hasCollidedX) {
			hasCollidedX = level->GetBlockAt(static_cast<int>(position.x - bb.Extents.x), static_cast<int>(lastPosition.z - bb.Extents.z))->HasCollision();
			hasCollidedX |= level->GetBlockAt(static_cast<int>(position.x + bb.Extents.x), static_cast<int>(lastPosition.z + bb.Extents.z))->HasCollision();
			hasCollidedX |= level->GetBlockAt(static_cast<int>(position.x - bb.Extents.x), static_cast<int>(lastPosition.z + bb.Extents.z))->HasCollision();
			hasCollidedX |= level->GetBlockAt(static_cast<int>(position.x + bb.Extents.x), static_cast<int>(lastPosition.z - bb.Extents.z))->HasCollision();
			if (hasCollidedX)
				position.x = lastPosition.x;
		}

		if (!hasCollidedZ) {
			hasCollidedZ = level->GetBlockAt(static_cast<int>(lastPosition.x - bb.Extents.x), static_cast<int>(position.z - bb.Extents.z))->HasCollision();
			hasCollidedZ |= level->GetBlockAt(static_cast<int>(lastPosition.x + bb.Extents.x), static_cast<int>(position.z + bb.Extents.z))->HasCollision();
			hasCollidedZ |= level->GetBlockAt(static_cast<int>(lastPosition.x - bb.Extents.x), static_cast<int>(position.z + bb.Extents.z))->HasCollision();
			hasCollidedZ |= level->GetBlockAt(static_cast<int>(lastPosition.x + bb.Extents.x), static_cast<int>(position.z - bb.Extents.z))->HasCollision();
			if (hasCollidedZ)
				position.z = lastPosition.z;
		}

		OnLevelCollision(hasCollidedX, hasCollidedZ);
	}
}


void Entity::HandleEntityCollision() {

	//SUPER TEMPORARY FIX - Projectiles/other entities try to check for colissions when the level is unreachable(after death), 
	//probably some kind of re-ordering needed. a bit beyond me
	//FIXME
	if (!level) {
		return;
	}
	// list of blocks we will need to check for collision with entities
	Block* blockCollisionList[] =
	{
		level->GetBlockAt(position.x,position.z),
		level->GetBlockAt(position.x - 1,position.z),
		level->GetBlockAt(position.x + 1,position.z),
		level->GetBlockAt(position.x,position.z + 1),
		level->GetBlockAt(position.x,position.z - 1),
		level->GetBlockAt(position.x + 1,position.z + 1),
		level->GetBlockAt(position.x + 1,position.z - 1),
		level->GetBlockAt(position.x - 1,position.z + 1),
		level->GetBlockAt(position.x - 1,position.z - 1),
	};

	int size = sizeof(blockCollisionList) / sizeof(Block*);

	DirectX::BoundingBox myBox = GetBoundingBox();

	Entity* collidingEntity = nullptr;
	bool collisionX(false);
	bool collisionZ(false);
	
	for (int i = 0; i < size; ++i) {

		for (Entity* entity : blockCollisionList[i]->entities) {
			
			if (entity != this) {

				const DirectX::BoundingBox box = entity->GetBoundingBox();
				const DirectX::SimpleMath::Vector3 e_pos = entity->GetPosition();
				
				
				
				if (position.x + myBox.Extents.x >= e_pos.x - box.Extents.x &&
					position.x - myBox.Extents.x <= e_pos.x + box.Extents.x &&
					lastPosition.z + myBox.Extents.z >= e_pos.z - box.Extents.z &&
					lastPosition.z - myBox.Extents.z <= e_pos.z + box.Extents.z)
				{
					collisionX = true;
					collidingEntity = entity;
				}

				if (lastPosition.x + myBox.Extents.x >= e_pos.x - box.Extents.x &&
					lastPosition.x - myBox.Extents.x <= e_pos.x + box.Extents.x &&
					position.z + myBox.Extents.z >= e_pos.z - box.Extents.z &&
					position.z - myBox.Extents.z <= e_pos.z + box.Extents.z)
				{
					collisionZ = true;
					collidingEntity = entity;
				}

				Collision(collidingEntity, collisionX, collisionZ);
			}
		}


	}
}

float Entity::GetDistanceFromPlayer() const
{
	Entity* player = level->GetPlayer();
	return DirectX::SimpleMath::Vector3::Distance(player->GetPosition(), GetPosition());
}

void Entity::Collision(Entity * entityCollidedWith, bool inXAxis, bool inZAxis) {
	if (!dynamic_cast<Projectile*>(entityCollidedWith) && !dynamic_cast<Pickup*>(entityCollidedWith))
	{
		if (entityCollidedWith != nullptr) {
			if (!entityCollidedWith->isTriggerCollision && !isTriggerCollision)
			{
				if (inXAxis)
					position.x = lastPosition.x;
				if (inZAxis)
					position.z = lastPosition.z;
			}
		}
	}
}

void Entity::OnLevelCollision(bool inXAxis, bool inZAxis)
{
}

void Entity::Reset(const DirectX::SimpleMath::Vector3 & position_)
{
	// Removes from previous space
	std::vector<Entity*>* entities = &level->GetBlockAt(static_cast<int>(lastPosition.x), static_cast<int>(lastPosition.z))->entities;
	entities->erase(std::remove(entities->begin(), entities->end(), this), entities->end());

	this->position = position_;
	this->lastPosition = position;
}

void Entity::LinkEntityToMap(Level* level) {
	this->level = level;
	lastPosition = position;
}

void Entity::UnlinkEntityFromMap()
{
	// Removes from previous space
	{
		auto* entities = &level->GetBlockAt(static_cast<int>(lastPosition.x), static_cast<int>(lastPosition.z))->entities;
		entities->erase(std::remove(entities->begin(), entities->end(), this), entities->end());
	}
	// Removes from current space
	{
		auto* entities = &level->GetBlockAt(static_cast<int>(position.x), static_cast<int>(position.z))->entities;
		entities->erase(std::remove(entities->begin(), entities->end(), this), entities->end());
	}
}

DirectX::BoundingBox Entity::GetBoundingBox()
{
	return DirectX::BoundingBox();
}

void Entity::InitialiseModel(Model& model, const std::string& meshName, const std::string& filePath, const Vector3& scale, const Vector3&position, const Vector3& rotation)
{
	if (!GetMeshManager()->GetMesh(meshName))
	{
		Mesh& m = GetMeshManager()->CreateMesh(meshName);
		m.CreateFrom(filePath, gd3dDevice, FX::GetMyFX()->mCache);
		model.Initialise(m);
	}
	else
	{
		Mesh* m = GetMeshManager()->GetMesh(meshName);
		model.Initialise(*m);
	}

	model.GetScale() = scale;
	model.GetPosition() = position;
	model.GetRotation() = rotation;
}
