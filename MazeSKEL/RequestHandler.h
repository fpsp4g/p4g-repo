#ifndef RequestHandlerH
#define RequestHandlerH

#include "Pathfinder.h"
#include "PathRequest.h"
#include "Queue.h"

// Handle Pathfinding Requests
class RequestHandler
{
public:
	RequestHandler()
	{
		_ASSERT(instance == nullptr);
		instance = this;
		new Pathfinder;
	}
	~RequestHandler()
	{
		instance = nullptr;
	}
	static RequestHandler* GetInstance()
	{
		_ASSERT(instance);
		return instance;
	}
	void AddRequestToQueue(Node, Node, Queue<Node>*, bool*);
	void ProcessNextRequest();
	void DrawLine(Node start, Node target, bool* canSearch, bool* found) { lineRequests.addToEnd(LineRequest{ start, target, canSearch, found }); }
	void release();
private:
	
	static RequestHandler* instance;
	Queue<PathRequest> requests;
	Queue<LineRequest> lineRequests;

};
#endif