#include "RangedEnemy.h"

RangedEnemy::RangedEnemy(Vector3 pos, string model, int hp) : Enemy(model, hp, Vector3(pos.x, 0.5, pos.z), Vector3(0.005f, 0.005f, 0.005f), Vector3(0, 0, 0), 1)
{

}
RangedEnemy::~RangedEnemy()
{

}
void RangedEnemy::SearchBehaviour(float dTime)
{
	if (CanSeeTarget(level->GetPlayerNode())) //if you can see the player...
	{
		state = EnemyState::CHASE;
	}
	else
	{
		if (!pathRequested) //if the enemy hasn't asked for a path yet, do it
		{
			PathTo(level->GetPlayerNode());
			pathRequested = true;
		}
		else if (pathFound)
		{
			//move towards the next node
			Vector3 nodePos(currentPath.first().getX() + 0.5, GetPosition().y, currentPath.first().getY() + 0.5);
			MoveTowards(nodePos, dTime);

			if (Vector3::Distance(nodePos, GetPosition()) < 0.1)
			{
				currentPath.removeFirst();
				if (currentPath.length() == 0) //if the enemy has finished searching the path, give up looking
				{
					pathRequested = false;
					pathFound = false;
					state = EnemyState::PATROL;
				}
			}
		}
	}
}
void RangedEnemy::PatrolBehaviour(float dTime)
{
	if (!CanSeeTarget(level->GetPlayerNode())) //if you can't see the player...
	{
		MoveTowards(Vector3(spawnPos.x, GetPosition().y, spawnPos.z), dTime);

		if (Vector3::Distance(spawnPos, GetPosition()) < 0.1)
		{
			state = EnemyState::IDLE;
		}
	}
	else
	{
		state = EnemyState::CHASE;
	}
}
void RangedEnemy::IdleBehaviour(float dTime)
{
	if (CanSeeTarget(level->GetPlayerNode())) //if you can see the player...
	{
		state = EnemyState::CHASE;	//give chase
	}
}
void RangedEnemy::AttackBehaviour(float dTime)
{
	//ATTACK
	EnemyProjectile* bullet = new EnemyProjectile(Vector3(position.x, position.y - 0.2, position.z), Vector3(0,GetYRotation(),0), forward);

	if (GetIAudioMgr()->GetSfxMgr()->IsPlaying(shotSound) == false)
		GetIAudioMgr()->GetSfxMgr()->Play("275151__bird-man__gun-shot", false, false, &shotSound, 0.5 / (level->GetPlayer()->GetPosition() - position).Length());
	else
		GetIAudioMgr()->GetSfxMgr()->Stop(shotSound);

	level->GetBlockAt(position.x+1, position.z)->sprites.push_back(bullet);//stops the rendering iterator becoming invalid
	bullet->LinkEntityToMap(level);

	//===================
	state = EnemyState::CHASE;
}
void RangedEnemy::ChaseBahaviour(float dTime)
{
	if (CanSeeTarget(level->GetPlayerNode())) //if you can see the player...
	{
		//move towards the player
		MoveTowards(Vector3(level->GetPlayerNode().getX() + 0.5, GetPosition().y, level->GetPlayerNode().getY() + 0.5), dTime);
		if (attackTimer <= 0) 
		{
			attackTimer = maxAttackTime;
			state = EnemyState::ATTACK;
		}
		else
		{
			attackTimer -= dTime;
		}
	}
	else
	{
		attackTimer = 0;
		state = EnemyState::SEARCH;
	}
}

void RangedEnemy::Die()
{
	Enemy::Die();

	// Adds exploding particles
	ExplodeEmitter* pSystem = new ExplodeEmitter(position.x, position.z);
	level->GetBlockAt(-1,-1)->sprites.push_back(pSystem);
	pSystem->LinkEntityToMap(level);
}
