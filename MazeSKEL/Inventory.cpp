#include "Inventory.h"

// Weapons
#include "Gauntlets.h"
#include "Bouncer.h"


Inventory::Inventory()
{
	InitialiseWeapons();
}
Inventory::~Inventory()
{
	ReleaseWeapons();
}

void Inventory::Update(float dTime)
{
	if (swapDelay > 0)
		swapDelay -= dTime;
	else if (swapDelay != 0)
		swapDelay = 0;
}

// Weapons
void Inventory::InitialiseWeapons()
{
	// Gauntlets
	Gauntlets* gntlts = new Gauntlets("Some Robot's Stolen Hands");
	weapons.push_back(gntlts);

	currentWeaponCounter = 0;
	currentWeapon = weapons[currentWeaponCounter];
	currentWeapon->SetEquipped(true);
}

void Inventory::NextWeapon()
{
	if (swapDelay <= 0)
	{
		currentWeaponCounter++;
		if (currentWeaponCounter > weapons.size() - 1)
			currentWeaponCounter = 0;
		SwapWeapons();
	}
}
void Inventory::PreviousWeapon()
{
	if (swapDelay <= 0)
	{
		currentWeaponCounter--;
		if (currentWeaponCounter < 0)
			currentWeaponCounter = weapons.size() - 1;
		SwapWeapons();
	}
}
void Inventory::SwapWeapons()
{
	swapDelay = weaponSwapCooldown;
	currentWeapon->SetEquipped(false);
	currentWeapon = weapons[currentWeaponCounter];
	currentWeapon->SetEquipped(true);
}

void Inventory::AddWeapon(string weaponName)
{
	if(weaponName ==  "Bouncer")
	{
		Bouncer * pabhc = new Bouncer("Pump-Action Bouncy Hopper Cannon");
		weapons.push_back(pabhc);
	}

	currentWeaponCounter = weapons.size() - 1;
	SwapWeapons();
}

void Inventory::ReleaseWeapons()
{
	auto it = weapons.begin();
	while (it != weapons.end())
	{
		Weapon* temp = (*it);
		it = weapons.erase(it);
		temp->Release();
		delete temp;
	}
}

// Other stuff