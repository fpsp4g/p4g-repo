#include "SpecialTileBlock.h"

void SpecialTileBlock::SetFloorTexture(int textureID) {
	if (floorTextureID != textureID) {
		requiresUpdate = true;
		floorTextureID = textureID;
	}
}

void SpecialTileBlock::SetFloorColor(long color) {
	if (floorColour != color) {
		requiresUpdate = true;
		floorColour = color;
	}
}