#ifndef LEVEL2H
#define LEVEL2H

#include "Level.h"

class Level2 : public Level {

public:
	void Init();
	virtual void SwitchLevel(int id);

};

#endif