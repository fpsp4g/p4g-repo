#include "TurretEnemy.h"

TurretEnemy::TurretEnemy(Vector3 pos)
	: Enemy("Turret", 100, pos, Vector3(0.005f, 0.005f, 0.005f), Vector3(PI/2, 0, 0), 1)
{

}
TurretEnemy::~TurretEnemy()
{

}
void TurretEnemy::SearchBehaviour(float dTime)
{
	state = EnemyState::IDLE;	//there is no reason for a turretEnemy to be searching
}
void TurretEnemy::PatrolBehaviour(float dTime)
{
	state = EnemyState::IDLE;	//there is no reason for a turretEnemy to be patrolling
}
void TurretEnemy::IdleBehaviour(float dTime)
{
	if (CanSeeTarget(level->GetPlayerNode())) //if you can see the player...
	{
		if (attackTimer <= 0)
		{
			attackTimer = maxAttackTime;
			state = EnemyState::ATTACK;
		}
		else
		{
			attackTimer -= dTime;
		}
	}
}
void TurretEnemy::AttackBehaviour(float dTime)
{
	//ATTACK
	EnemyProjectile* bullet = new EnemyProjectile(Vector3(position.x, position.y + 0.6, position.z), Vector3(0, GetYRotation(), 0), forward);

	if(GetIAudioMgr()->GetSfxMgr()->IsPlaying(shotSound) == false)
		GetIAudioMgr()->GetSfxMgr()->Play("275151__bird-man__gun-shot", false, false, &shotSound, 0.5 / (level->GetPlayer()->GetPosition() - position).Length());
	else
		GetIAudioMgr()->GetSfxMgr()->Stop(shotSound);

	level->GetBlockAt(position.x+1, position.z)->sprites.push_back(bullet);	//stops the rendering iterator becoming invalid
	bullet->LinkEntityToMap(level);

	//bullet = new EnemyProjectile(Vector3(position.x, position.y + 0.5, position.z), Vector3(0, GetYRotation() + D2R(5), 0), XMVector2TransformNormal(forward, Matrix::CreateRotationY(D2R(5))));
	//level->GetBlockAt(position.x + 1, position.z)->sprites.push_back(bullet);
	//bullet->LinkEntityToMap(level);

	//bullet = new EnemyProjectile(Vector3(position.x, position.y + 0.5, position.z), Vector3(0, GetYRotation() - D2R(5), 0), XMVector2TransformNormal(forward, Matrix::CreateRotationY(D2R(-5))));
	//level->GetBlockAt(position.x + 1, position.z)->sprites.push_back(bullet);
	//bullet->LinkEntityToMap(level);

	//===================
	state = EnemyState::IDLE;
}
void TurretEnemy::ChaseBahaviour(float dTime)
{
	state = EnemyState::IDLE;	//there is no reason for a turretEnemy to be chasing
}

void TurretEnemy::Die()
{
	Enemy::Die();

	// Adds exploding particles
	ExplodeEmitter* pSystem = new ExplodeEmitter(position.x, position.z);
	level->GetBlockAt(-1, -1)->sprites.push_back(pSystem);
	pSystem->LinkEntityToMap(level);
}
