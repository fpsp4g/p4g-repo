
#include "PSCore.hlsl"

float4 main(VertexOut pin) : SV_Target
{

	float4 pixelColour = PSCore((VertexOut)pin, true);

	return ApplyFog(pin, pixelColour);
}

