
#include "PSCore.hlsl"

float4 main(TomVertexOut pin) : SV_Target
{
	float4 pixelColour = PSCore((VertexOut)pin, true) * float4(pin.Colour.rgb,1.0);

	return ApplyFog((VertexOut)pin, pixelColour);
}

