
#include "PSCore.hlsl"

float4 main(VertexOut pin) : SV_Target
{

	float4 pixelColour = PSCore(pin, false);

	return ApplyFog(pin, pixelColour);
}

